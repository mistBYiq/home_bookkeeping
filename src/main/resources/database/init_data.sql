insert into categories as c (name, description) values ('PRODUCTS', 'PRODUCTS');
insert into categories as c (name, description) values ('ENTERTAINMENT', 'ENTERTAINMENT');
insert into categories as c (name, description) values ('SUBJECTS', 'SUBJECTS');
insert into categories as c (name, description) values ('SERVICES', 'SERVICES');
insert into categories as c (name, description) values ('HEALTHS', 'HEALTHS');
insert into categories as c (name, description) values ('BEAUTY', 'BEAUTY');
insert into categories as c (name, description) values ('NOT_SELECTED', 'NOT_SELECTED');

insert into subcategories as s (name, description, category_name) values ('SALON', 'SALON', 'BEAUTY');
insert into subcategories as s (name, description, category_name) values ('MANICURE', 'MANICURE', 'BEAUTY');
insert into subcategories as s (name, description, category_name) values ('DEPILATION', 'DEPILATION', 'BEAUTY');
insert into subcategories as s (name, description, category_name) values ('OTHER_BEAUTY', 'OTHER', 'BEAUTY');

insert into subcategories as s (name, description, category_name) values ('PRESENT', 'PRESENT', 'ENTERTAINMENT');
insert into subcategories as s (name, description, category_name) values ('CINEMA', 'CINEMA', 'ENTERTAINMENT');
insert into subcategories as s (name, description, category_name) values ('TRAVEL', 'TRAVEL', 'ENTERTAINMENT');
insert into subcategories as s (name, description, category_name) values ('CAFE', 'CAFE', 'ENTERTAINMENT');
insert into subcategories as s (name, description, category_name) values ('OTHER_ENTERTAINMENT', 'OTHER', 'ENTERTAINMENT');

insert into subcategories as s (name, description, category_name) values ('MEDICAMENT', 'MEDICAMENT', 'HEALTHS');
insert into subcategories as s (name, description, category_name) values ('HYGIENE', 'HYGIENE', 'HEALTHS');
insert into subcategories as s (name, description, category_name) values ('VITAMINS', 'VITAMINS', 'HEALTHS');
insert into subcategories as s (name, description, category_name) values ('OTHER_HEALTHS', 'OTHER', 'HEALTHS');

insert into subcategories as s (name, description, category_name) values ('COMMUNAL', 'COMMUNAL', 'SERVICES');
insert into subcategories as s (name, description, category_name) values ('HOUSING', 'HOUSING', 'SERVICES');
insert into subcategories as s (name, description, category_name) values ('TRANSPORT', 'TRANSPORT', 'SERVICES');
insert into subcategories as s (name, description, category_name) values ('INTERNET', 'INTERNET', 'SERVICES');
insert into subcategories as s (name, description, category_name) values ('MOBILE_CONNECTION', 'MOBILE_CONNECTION', 'SERVICES');
insert into subcategories as s (name, description, category_name) values ('OTHER_SERVICES', 'OTHER', 'SERVICES');

insert into subcategories as s (name, description, category_name) values ('FURNITURE', 'FURNITURE', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('CLOTHES', 'CLOTHES', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('SHOES', 'SHOES', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('TECHNIQUE', 'TECHNIQUE', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('ELECTRONICS', 'ELECTRONICS', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('DISHES', 'DISHES', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('HOUSEHOLD_CHEMICALS', 'HOUSEHOLD_CHEMICALS', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('PACKAGE', 'PACKAGE', 'SUBJECTS');
insert into subcategories as s (name, description, category_name) values ('OTHER_SUBJECTS', 'OTHER', 'SUBJECTS');

insert into subcategories as s (name, description, category_name) values ('NOT_SELECTED', 'NOT_SELECTED', 'NOT_SELECTED');

insert into subcategories as s (name, description, category_name) values ('TASTY', 'ВКУСОВЫЕ: безалкогольные напитки, чай, кофе, пряности, приправы', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('EDIBLE_FATS_SAUCES_DRESSING', 'ПИЩЕВЫЕ ЖИРЫ,СОУСЫ И ЗАПРАВКИ: растительные масла, животные топленые жиры, кулинарные, кондитерские и хлебопекарные жиры, маргарин, майонез', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('EGG', 'ЯИЧНЫЕ: яйцо куриное, мороженые яичные продукты, яичные порошки', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('FISH', 'РЫБНЫЕ: рыба живая, охлажденная и мороженая, соленые, вяленые, сушеные, копченые рыбные товары, консервы и пресервы, полуфабрикаты и кулинарные изделия, икра', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('MEAT', 'МЯСНЫЕ:мясо и субпродукты, мясо птицы, мясные полуфабрикаты и кулинарные изделия, мясные консервы, мясокопчености и колбасные изделия', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('DAIRY', 'МОЛОЧНЫЕ: молоко, сливки, кисломолочные продукты, масло сливочное, сыры, мороженое, молочные консервы и молочные продукты для детского и диетического питания', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('CONFECTIONERY', 'КОНДИТЕРСКИЕ: мармелад, карамель, конфеты, шоколад, драже, ирис, халва, печенье, пряники, вафли, кексы, рулеты и ромовые бабы, торты и пирожные', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('SUGAR_HONEY_SUBSTITUTES', 'САХАР, МЁД, ЗАМЕНИТЕЛИ: сахар-песок, мёд, сахар-рафинад, заменители сахара - ксилит, сорбит', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('FRUIT', 'ПЛОДОВЫЕ: свежие плоды, продукты их переработки: квашеные, соленые, моченые, маринованные, сушеные, быстрозамороженные, консервированные плоды', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('VEGETABLES', 'ОВОЩНЫЕ: овощи и грибы, продукты их переработки: квашеные, соленые, моченые, маринованные, сушеные, быстрозамороженные, консервированные овощи и грибы', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('GRAINS', 'ЗЕРНОВЫЕ: зерно, крупа', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('FLOUR', 'МУЧНЫЕ: мука, макаронные изделия, хлебобулочные, сухарные и бараночные изделия', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('TOBACCO', 'ТАБАК: табачные изделия', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('ALCOHOL', 'АЛКОГОЛЬ: алкогольная продукция', 'PRODUCTS');
insert into subcategories as s (name, description, category_name) values ('OTHER_PRODUCTS', 'OTHER', 'PRODUCTS');


insert into card_products as cp (name, description, subcategory_name) values ('напиток', 'напиток', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('вода', 'вода', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('минеральная вода', 'минеральная вода', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('сок', 'сок', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('нектар', 'нектар', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('чай', 'чай', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('кофе', 'кофе', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('какао', 'какао', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('специи', 'специи', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('соль', 'соль', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('лимонная кислота', 'лимонная кислота', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('уксус', 'уксус', 'TASTY');
insert into card_products as cp (name, description, subcategory_name) values ('дрожжи', 'дрожжи', 'TASTY');

insert into card_products as cp (name, description, subcategory_name) values ('подсолнечное масло', 'подсолнечное масло', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('оливковое масло', 'оливковое масло', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('соевый соус', 'соевый соус', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('майонез', 'майонез', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('кетчуп', 'кетчуп', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('маргарин', 'маргарин', 'EDIBLE_FATS_SAUCES_DRESSING');
insert into card_products as cp (name, description, subcategory_name) values ('соус', 'соус', 'EDIBLE_FATS_SAUCES_DRESSING');

insert into card_products as cp (name, description, subcategory_name) values ('яйцо куриное', 'яйца столовые', 'EGG');
insert into card_products as cp (name, description, subcategory_name) values ('яичный порошок', 'яичный порошок', 'EGG');
insert into card_products as cp (name, description, subcategory_name) values ('копченые яйца', 'яйца копченые', 'EGG');
insert into card_products as cp (name, description, subcategory_name) values ('вареные яйца', 'яйца вареные', 'EGG');

insert into card_products as cp (name, description, subcategory_name) values ('рыба охл', 'охл', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('рыба зам', '', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('рыбные бургеры зам', 'зам', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('рыбные палочки зам', 'зам', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('рыбные консервы', 'консервы', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('рыбные пресервы', 'пресервы', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('икра', 'охл', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('вяленая', 'охл', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('копченая', 'охл', 'FISH');
insert into card_products as cp (name, description, subcategory_name) values ('соленая', 'охл', 'FISH');


insert into card_products as cp (name, description, subcategory_name) values ('курица охл', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('свинина охл', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('говядина охл', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('баранина охл', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('курица зам', 'зам', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('свинина зам', 'зам', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('говядина зам', 'зам', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('баранина зам', 'зам', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('колбаса', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('колбаса охл', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('сосиски', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('сардельки', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('пельмени', 'зам', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('рулет мясной', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('паштет', 'охл', 'MEAT');
insert into card_products as cp (name, description, subcategory_name) values ('полуфабрикат мясной', 'полуфабрикат', 'MEAT');


insert into card_products as cp (name, description, subcategory_name) values ('молоко', 'молоко', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('кефир', 'кефир', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сливочное масло', 'масло', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сливки', 'сливки', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сметана', 'сметана', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сыр', 'сыр', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('мороженое', 'мороженое', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('ряженка', 'ряженка', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сыворотка', 'сыворотка', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('йогурт', 'йогурт', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('творог', 'творог', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('мусс', 'мусс', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сухое молоко', 'сухое молоко', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('сгущенка', 'сгущенка', 'DAIRY');
insert into card_products as cp (name, description, subcategory_name) values ('творожная масса', 'творожная масса', 'DAIRY');


insert into card_products as cp (name, description, subcategory_name) values ('пряники', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('пироженое', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('торт', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('рулет', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('кекс', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('драже', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('шоколад', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('конфеты', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('карамель', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('батончик', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('печенье', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('вафли', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('ирис', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('мармелад', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('халва', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('зефир', 'сладости', 'CONFECTIONERY');
insert into card_products as cp (name, description, subcategory_name) values ('пастила', 'сладости', 'CONFECTIONERY');

insert into card_products as cp (name, description, subcategory_name) values ('сахар', 'сахар', 'SUGAR_HONEY_SUBSTITUTES');
insert into card_products as cp (name, description, subcategory_name) values ('мед', 'мед', 'SUGAR_HONEY_SUBSTITUTES');
insert into card_products as cp (name, description, subcategory_name) values ('сусли', 'сусли', 'SUGAR_HONEY_SUBSTITUTES');
insert into card_products as cp (name, description, subcategory_name) values ('сахарная пудра', 'сахарная пудра', 'SUGAR_HONEY_SUBSTITUTES');
insert into card_products as cp (name, description, subcategory_name) values ('сахарная вата', 'сахарная вата', 'SUGAR_HONEY_SUBSTITUTES');

insert into card_products as cp (name, description, subcategory_name) values ('яблоко', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('банан', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('изюм', 'сухофрукт', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('виноград', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('лимон', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('апельсин', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('мандарин', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('ананас', 'свежий', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('ананас консервированный', 'консервация', 'FRUIT');
insert into card_products as cp (name, description, subcategory_name) values ('черешня', 'свежий', 'FRUIT');

insert into card_products as cp (name, description, subcategory_name) values ('картофель', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('фасоль консервированная', 'фасоль консервированная', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('капуста', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('свекла', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('морковь', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('лук', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('чеснок', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('томаты', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('огурец', 'свежий', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('горох колотый', 'горох колотый', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('смесь замороженная', 'замороженная', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('оливки консервированныя', 'консервация', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('грибы консервированныя', 'консервация', 'VEGETABLES');
insert into card_products as cp (name, description, subcategory_name) values ('грибы замороженные', 'замороженные', 'VEGETABLES');

insert into card_products as cp (name, description, subcategory_name) values ('гречка', 'гречка', 'GRAINS');
insert into card_products as cp (name, description, subcategory_name) values ('первовая крупа', 'первовая крупа', 'GRAINS');
insert into card_products as cp (name, description, subcategory_name) values ('рис', 'рис', 'GRAINS');
insert into card_products as cp (name, description, subcategory_name) values ('пшено', 'пшено', 'GRAINS');
insert into card_products as cp (name, description, subcategory_name) values ('ячневая крупа', 'ячневая крупа', 'GRAINS');

insert into card_products as cp (name, description, subcategory_name) values ('вино', 'виноградные вина', 'ALCOHOL');
insert into card_products as cp (name, description, subcategory_name) values ('пиво', 'пиво', 'ALCOHOL');
insert into card_products as cp (name, description, subcategory_name) values ('джин', 'джин', 'ALCOHOL');
insert into card_products as cp (name, description, subcategory_name) values ('виски', 'виски', 'ALCOHOL');

insert into card_products as cp (name, description, subcategory_name) values ('сигареты', 'сигареты', 'TOBACCO');

insert into card_products as cp (name, description, subcategory_name) values ('мука', 'мука', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('макароны', 'макароны', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('хлеб', 'хлеб', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('багет', 'багет', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('сушки', 'сушки', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('баранки', 'баранки', 'FLOUR');
insert into card_products as cp (name, description, subcategory_name) values ('сухари', 'сухари', 'FLOUR');