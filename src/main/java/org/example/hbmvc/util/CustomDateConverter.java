package org.example.hbmvc.util;

import lombok.RequiredArgsConstructor;
import org.example.hbmvc.exception.ValidateDateException;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class CustomDateConverter {

    public LocalDate convertStringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        // unchecked java.time.format.DateTimeParseException
        return LocalDate.parse(dateString, formatter);
    }

    public Date convertStringToDate(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Date date = null;
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new ValidateDateException("Exception The date is not in the correct format");
        }
        return date;
    }
}
