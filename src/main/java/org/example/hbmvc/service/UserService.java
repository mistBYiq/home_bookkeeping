package org.example.hbmvc.service;

import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.user.UserCreateRequest;
import org.example.hbmvc.dto.user.UserDto;
import org.example.hbmvc.dto.user.UserRegistrationDto;
import org.example.hbmvc.dto.user.UserUpdateRequest;
import org.example.hbmvc.dto.user.UserFullDto;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<User> findAll();

    Page<UserDto> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    List<UserDto> findAllUsers();

    UserFullDto findUserFullById(Long id);

    UserDto findUserById(Long id);

    UserDto findUserByEmail(String email);

    UserDto create(UserCreateRequest request);

    User saveRegistered(UserRegistrationDto registrationDto);

    void save(UserDto userDto);

    UserDto updateById(Long id, UserCreateRequest request);

    void update(UserUpdateRequest request);

    void deleteById(Long id);
}
