package org.example.hbmvc.service;


import org.example.hbmvc.domain.entity.plan.Item;
import org.example.hbmvc.dto.item.ItemCreateRequest;
import org.example.hbmvc.dto.item.ItemDto;
import org.example.hbmvc.dto.item.ItemUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ItemService {

    List<Item> findAll();

    Page<Item> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    List<ItemDto> findAllItems();

    ItemDto create(ItemCreateRequest request);

    ItemDto create(ItemCreateRequest request, Long buyListId);

    ItemDto update(ItemUpdateRequest request);

    ItemDto findById(Long id);

    void deleteById(Long id);

}
