package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.prod.CardProduct;
import org.example.hbmvc.domain.entity.prod.Subcategory;
import org.example.hbmvc.dto.cardproduct.CardProductCreateRequest;
import org.example.hbmvc.dto.cardproduct.CardProductDto;
import org.example.hbmvc.dto.cardproduct.CardProductFullDto;
import org.example.hbmvc.dto.cardproduct.CardProductUpdateRequest;
import org.example.hbmvc.exception.CardProductEntityNotFoundException;
import org.example.hbmvc.exception.SubcategoryEntityNotFoundException;
import org.example.hbmvc.repository.CardProductRepository;
import org.example.hbmvc.repository.SubcategoryRepository;
import org.example.hbmvc.service.CardProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class CardProductServiceImpl implements CardProductService {

    private static final String ERR_MESSAGE = "CardProduct not found with id =";

    @Autowired
    private CardProductRepository cardProductRepository;

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Transactional
    @Override
    public CardProductDto createCardProduct(CardProductCreateRequest request) {
        CardProduct cardProduct = buildCardProduct(request);

        return CardProductDto.toDto(cardProductRepository.save(cardProduct));
    }

    private CardProduct buildCardProduct(CardProductCreateRequest request) {
        Subcategory subcategory = subcategoryRepository.findByName(request.getSubcategoryName()).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with name=" + request.getSubcategoryName())
        );
        CardProduct entity = new CardProduct();
        entity.setSubcategory(subcategory);
        entity.setName(request.getName().toLowerCase());
        entity.setDescription(request.getDescription().toLowerCase());
        entity.setCreated(LocalDateTime.now());
        entity.setUpdated(LocalDateTime.now());
        entity.setIsDeleted(Boolean.FALSE);
        entity.setCounterUse(1);

        return entity;
    }

    @Transactional
    @Override
    public void create(CardProductCreateRequest request) {
        CardProduct cardProduct = buildCardProduct(request);
        cardProductRepository.save(cardProduct);
    }

    @Override
    public CardProductFullDto findProductFullDtoById(Long id) {
        return CardProductFullDto.toDto(cardProductRepository.findById(id).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public CardProductDto findProductDtoById(Long id) {
        return CardProductDto.toDto(cardProductRepository.findById(id).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public CardProductDto findByName(String name) {
        return CardProductDto.toDto(cardProductRepository.findByName(name).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + name)
        ));
    }

    @Transactional
    @Override
    public void update(CardProductUpdateRequest request) {
        CardProduct entity = cardProductRepository.findById(request.getId()).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        cardProductUpdate(entity, request);
        Subcategory subcategory = subcategoryRepository.findByName(request.getSubcategoryName()).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with name=" + request.getSubcategoryName())
        );
        entity.setSubcategory(subcategory);
        cardProductRepository.save(entity);
    }

    private void cardProductUpdate(@NotNull CardProduct entity, @NotNull CardProductUpdateRequest request) {
        ofNullable(request.getName().toLowerCase()).map(entity::setName);
        ofNullable(request.getDescription().toLowerCase()).map(entity::setDescription);
        entity.setUpdated(LocalDateTime.now());
        //subcategory
    }

    @Transactional
    @Override
    public CardProductDto updateCardProduct(CardProductUpdateRequest request) {
        CardProduct entity = cardProductRepository.findById(request.getId()).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        cardProductUpdate(entity, request);
        return CardProductDto.toDto(cardProductRepository.save(entity));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        CardProduct entity = cardProductRepository.findById(id).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + id)
        );
        cardProductRepository.delete(entity);
    }

    @Transactional
    @Override
    public void deleteByName(String name) {
        CardProduct entity = cardProductRepository.findByName(name).orElseThrow(
                () -> new CardProductEntityNotFoundException(ERR_MESSAGE + name)
        );
        cardProductRepository.delete(entity);
    }

    @Override
    public List<CardProductDto> findAll() {
        return cardProductRepository.findAll()
                .stream()
                .map(CardProductDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CardProductDto> findAllByNameLike(String query) {
        List<CardProduct> list = query != null && !query.isEmpty() ?
                cardProductRepository.findAllByNameLike("%" + query.toLowerCase() + "%")
                :
                cardProductRepository.findAll();

        return list.stream()
                .map(CardProductDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Page<CardProductDto> findAllByNameLikePaging(String query,
                                                        int pageNo,
                                                        int pageSize,
                                                        String sortField,
                                                        String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<CardProduct> list = query != null && !query.isEmpty() ?
                cardProductRepository.findAllByNameLike("%" + query + "%", pageable)
                :
                cardProductRepository.findAll(pageable);

        return new PageImpl<>(list.stream()
                .map(CardProductDto::toDto)
                .collect(Collectors.toList()));
    }

}
