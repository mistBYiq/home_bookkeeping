package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.buylist.BuyListCreateRequest;
import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.buylist.BuyListFullDto;
import org.example.hbmvc.dto.buylist.BuyListUpdateRequest;
import org.example.hbmvc.exception.BuyListEntityNotFoundException;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.BuyListRepository;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.BuyListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class BuyListServiceImpl implements BuyListService {

    private static final String ERR_MESSAGE = "BuyList not found with id =";

    @Autowired
    private BuyListRepository buyListRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<BuyList> findAll() {
        return buyListRepository.findAll();
    }

    @Override
    public Page<BuyList> findPaginated(int pageNo,
                                       int pageSize,
                                       String sortField,
                                       String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.buyListRepository.findAll(pageable);
    }

    @Override
    public BuyListDto findBuyListById(Long id) {
        return BuyListDto.toDto(buyListRepository.findById(id).orElseThrow(
                () -> new BuyListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public BuyListFullDto findBuyListFullById(Long id) {
        return BuyListFullDto.toDto(buyListRepository.findById(id).orElseThrow(
                () -> new BuyListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public List<BuyListDto> findAllLists() {
        return buyListRepository.findAll()
                .stream()
                .map(BuyListDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<BuyListDto> findAllFavoritesByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + userId)
        );
        return buyListRepository.findAllByUserAndIsFavouritesIsTrue(user)
                .stream()
                .map(BuyListDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<BuyListDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + userId)
        );
        return buyListRepository.findAllByUser(user)
                .stream()
                .map(BuyListDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public BuyListFullDto create(BuyListCreateRequest request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + request.getUserId())
        );
        BuyList buyList = buildBuyList(request, user);
        return BuyListFullDto.toDto(buyListRepository.save(buyList));
    }

    @Transactional
    @Override
    public BuyListFullDto update(BuyListUpdateRequest request) {
        BuyList buyList = buyListRepository.findById(request.getId()).orElseThrow(
                () -> new BuyListEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        buyListUpdate(buyList, request);
        return BuyListFullDto.toDto(buyListRepository.save(buyList));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        buyListRepository.findById(id).orElseThrow(
                () -> new BuyListEntityNotFoundException(ERR_MESSAGE + id)
        );
        buyListRepository.deleteById(id);
    }


    private BuyList buildBuyList(BuyListCreateRequest request, User user) {
        return new BuyList()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setUser(user)
                .setTotalPrice(BigDecimal.ZERO)
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE)
                .setIsFavourites(Boolean.FALSE)
                .setIsDone(Boolean.FALSE);
    }

    private void buyListUpdate(@NotNull BuyList buyList, @NotNull BuyListUpdateRequest request) {
        ofNullable(request.getName()).map(buyList::setName);
        ofNullable(request.getDescription()).map(buyList::setDescription);
        ofNullable(request.getIsFavorites()).map(buyList::setIsFavourites);
        buyList.setUpdated(LocalDateTime.now());
        //buyList.setTotalPrice(countSum(buyList.getProducts()));
    }

//    private Integer countSum(List<Goods> goods) {
//        Integer result = 0;
//        for (Goods goods : goods) {
//            result += goods.getPrice();
//        }
//        return result;
//    }
}
