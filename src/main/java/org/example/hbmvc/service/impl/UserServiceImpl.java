package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.domain.entity.user.UserRole;
import org.example.hbmvc.domain.enums.RoleName;
import org.example.hbmvc.dto.user.UserCreateRequest;
import org.example.hbmvc.dto.user.UserDto;
import org.example.hbmvc.dto.user.UserFullDto;
import org.example.hbmvc.dto.user.UserRegistrationDto;
import org.example.hbmvc.dto.user.UserUpdateRequest;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class UserServiceImpl implements UserService {

    private static final String ERR_MESSAGE = "User not found with id =";

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<UserDto> findPaginated(int pageNo,
                                       int pageSize,
                                       String sortField,
                                       String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return new PageImpl<>(this.userRepository.findAll(pageable)
                .stream()
                .map(UserDto::toDto)
                .collect(Collectors.toList()));
    }

    @Override
    public List<UserDto> findAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(UserDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserFullDto findUserFullById(Long id) {
        return UserFullDto.toDto(userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id)));
    }

    @Override
    public UserDto findUserById(Long id) {
        return UserDto.toDto(userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id)));
    }

    @Override
    public UserDto findUserByEmail(String email) {
        if (!userRepository.existsByEmail(email)) {
            throw new UserEntityNotFoundException("User email not found");
        }
        User user = userRepository.findUserByEmail(email);
        return UserDto.toDto(user);
    }

    @Override
    public User saveRegistered(UserRegistrationDto registrationDto) {
        User user = new User(registrationDto.getName(),
                registrationDto.getSurname(),
                registrationDto.getEmail(),
                passwordEncoder.encode(registrationDto.getPassword()),
                Arrays.asList(new UserRole(RoleName.ROLE_USER)));

        user.setCreated(LocalDateTime.now());
        user.setUpdated(LocalDateTime.now());
        user.setIsDeleted(Boolean.FALSE);
        user.setIsBlocked(Boolean.FALSE);

        return userRepository.save(user);
    }

    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            Optional<User> searchResult = userRepository.findByEmail(email);
            if (searchResult.isPresent()) {
                User user = searchResult.get();

                List<GrantedAuthority> roles = new ArrayList<>(mapRolesToAuthorities(user.getRoles()));

                return new org.springframework.security.core.userdetails.User(
                        user.getEmail(),
                        user.getPassword(),
                        roles
                );
            } else {
                throw new UsernameNotFoundException("User not found with email " + email);
            }
        } catch (Exception e) {
            throw new UsernameNotFoundException("User with this email not found");
        }
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<UserRole> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public UserDto create(UserCreateRequest createModel) {
        User user = buildUser(createModel);
        return UserDto.toDto(userRepository.save(user));
    }

    @Transactional
    @Override
    public void save(UserDto userDto) {
        User user = buildUser(userDto);
        userRepository.save(user);
    }

    @Transactional
    @Override
    public UserDto updateById(Long id, UserCreateRequest request) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id));
        userUpdate(user, request);
        return UserDto.toDto(userRepository.save(user));
    }

    @Transactional
    @Override
    public void update(UserUpdateRequest request) {
        User user = userRepository.findById(request.getId()).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + request.getId()));
        userUpdate(user, request);
        userRepository.save(user);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id));
        userRepository.deleteById(id);
    }

    private void userUpdate(@NotNull User user, @NotNull UserCreateRequest request) {
        ofNullable(request.getName()).map(user::setName);
        ofNullable(request.getSurname()).map(user::setSurname);
        ofNullable(request.getEmail()).map(user::setEmail);
//        ofNullable(request.getLogin()).map(user::setLogin);
        ofNullable(request.getPassword()).map(user::setPassword);
        user.setUpdated(LocalDateTime.now());
    }

    private User buildUser(UserCreateRequest model) {
        User user = new User();
        user.setName(model.getName());
        user.setSurname(model.getSurname());
        user.setEmail(model.getEmail());
//        user.setLogin(model.getLogin());
        user.setPassword(model.getPassword());
        user.setCreated(LocalDateTime.now());
        user.setUpdated(LocalDateTime.now());
        user.setIsDeleted(Boolean.FALSE);

//        UserRole userRole = new UserRole(RoleName.ROLE_ADMIN, user);
//
//        user.setUserRole(userRole);

        return user;
    }

    private User buildUser(UserDto model) {
        return new User()
                .setName(model.getName())
                .setSurname(model.getSurname())
                .setEmail(model.getEmail())
//                .setLogin(model.getLogin())
                .setPassword(model.getPassword())
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE);
    }
}
