package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.income.IncomeCreateRequest;
import org.example.hbmvc.dto.income.IncomeDto;
import org.example.hbmvc.dto.income.IncomeFullDto;
import org.example.hbmvc.dto.income.IncomeUpdateRequest;
import org.example.hbmvc.exception.IncomeEntityNotFoundException;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.IncomeRepository;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class IncomeServiceImpl implements IncomeService {

    private static final String ERR_MESSAGE = "Income not found with id =";

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<IncomeDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("Income Service. User not found with id=" + userId)
        );
        List<Income> incomes = incomeRepository.findAllByUser(user);
        return incomes
                .stream()
                .map(IncomeDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Income> findAll() {
        return null;
    }

    @Override
    public Page<Income> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        return null;
    }

    @Override
    public List<IncomeDto> findAllIncomes() {
        return incomeRepository.findAll()
                .stream()
                .map(IncomeDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public IncomeFullDto create(IncomeCreateRequest request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("Income Service. User not found with id=" + request.getUserId())
        );
        Income income = buildIncome(request, user);
        return IncomeFullDto.toDto(incomeRepository.save(income));
    }

    private Income buildIncome(IncomeCreateRequest request, User user) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return new Income()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setSum(request.getSum())
                .setUser(user)
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setDate(LocalDate.parse(request.getDate(), formatter))
                .setIsDeleted(Boolean.FALSE);
    }

    @Transactional
    @Override
    public IncomeFullDto update(IncomeUpdateRequest request) {
        Income income = incomeRepository.findById(request.getId()).orElseThrow(
                () -> new IncomeEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        incomeUpdate(income, request);
        return IncomeFullDto.toDto(incomeRepository.save(income));
    }


    private void incomeUpdate(@NotNull Income income, @NotNull IncomeUpdateRequest request) {
        ofNullable(request.getName()).map(income::setName);
        ofNullable(request.getDescription()).map(income::setDescription);
        ofNullable(request.getSum()).map(income::setSum);
        income.setUpdated(LocalDateTime.now());

    }


    @Override
    public IncomeFullDto findIncomeById(Long id) {
        return IncomeFullDto.toDto(incomeRepository.findById(id).orElseThrow(
                () -> new IncomeEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public void deleteById(Long id) {
        Income income = incomeRepository.findById(id).orElseThrow(
                () -> new IncomeEntityNotFoundException(ERR_MESSAGE + id)
        );
        incomeRepository.delete(income);
    }

    @Override
    public double countAllTotalSumByUserId(Long id) {
        return incomeRepository.countAllTotalSumByUserId(id);
    }
}
