package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.domain.entity.check.Goods;
import org.example.hbmvc.domain.entity.prod.CardProduct;
import org.example.hbmvc.dto.goods.GoodsCreateRequest;
import org.example.hbmvc.dto.goods.GoodsDto;
import org.example.hbmvc.dto.goods.GoodsFullDto;
import org.example.hbmvc.dto.goods.GoodsUpdateRequest;
import org.example.hbmvc.exception.CardProductEntityNotFoundException;
import org.example.hbmvc.exception.CheckListEntityNotFoundException;
import org.example.hbmvc.exception.GoodsEntityNotFoundException;
import org.example.hbmvc.repository.CardProductRepository;
import org.example.hbmvc.repository.CheckListRepository;
import org.example.hbmvc.repository.GoodsRepository;
import org.example.hbmvc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class GoodsServiceImpl implements GoodsService {

    private static final String ERR_MESSAGE = "Goods not found with id =";

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private CheckListRepository checkListRepository;

    @Autowired
    private CardProductRepository cardProductRepository;

    @Transactional
    @Override
    public GoodsDto createGoods(GoodsCreateRequest request) {
        Goods goods = buildGoods(request);
        return GoodsDto.toDto(goodsRepository.save(goods));
    }

    private Goods buildGoods(GoodsCreateRequest request) {
        CheckList checkList = checkListRepository.findById(request.getCheckListId()).orElseThrow(
                () -> new CheckListEntityNotFoundException("CheckList Not Found with id=" + request.getCheckListId())
        );
        CardProduct cardProduct = cardProductRepository.findByName(request.getCardProductName()).orElseThrow(
                () -> new CardProductEntityNotFoundException("CardProduct Not Found with name=" + request.getCheckListId())
        );

        Goods entity = new Goods();
        entity.setCardProduct(cardProduct);
        entity.setCheckList(checkList);

        entity.setName(request.getName());
        entity.setPrice(new BigDecimal(checkPrice(request.getPrice())));

        if (!request.getPriceWithoutDiscount().equals("")) {
            entity.setPriceWithoutDiscount(new BigDecimal(checkPrice(request.getPriceWithoutDiscount())));
        } else {
            entity.setPriceWithoutDiscount(new BigDecimal(checkPrice(request.getPrice())));
        }
        entity.setAmount(request.getAmount());

        entity.setMilliliters(request.getMilliliters());
        entity.setWeightInGrams(request.getWeightInGrams());
        entity.setComposition(request.getComposition());
        entity.setMadeIn(request.getMadeIn());
        entity.setDetailOne(request.getDetailOne());
        entity.setDetailTwo(request.getDetailTwo());
        entity.setDetailThree(request.getDetailThree());

        entity.setCreated(LocalDateTime.now());
        entity.setUpdated(LocalDateTime.now());
        entity.setIsDeleted(Boolean.FALSE);

        return entity;
    }


    private String checkPrice(String str) {
        if (str.contains(",")) {
            return str.replaceFirst(",", ".");
        }
        return str;
    }

    @Transactional
    @Override
    public void create(GoodsCreateRequest request) {
        Goods goods = buildGoods(request);
        goodsRepository.save(goods);
    }

    @Override
    public GoodsFullDto findGoodsFullDtoById(Long id) {
        return GoodsFullDto.toDto(goodsRepository.findById(id).orElseThrow(
                () -> new GoodsEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public GoodsDto findById(Long id) {
        return GoodsDto.toDto(goodsRepository.findById(id).orElseThrow(
                () -> new GoodsEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Transactional
    @Override
    public void update(GoodsUpdateRequest request) {
        Goods entity = goodsRepository.findById(request.getId()).orElseThrow(
                () -> new GoodsEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        goodsUpdate(entity, request);
    }

    private void goodsUpdate(@NotNull Goods entity, @NotNull GoodsUpdateRequest request) {
        ofNullable(request.getName()).map(entity::setName);
        Optional.of(new BigDecimal(checkPrice(request.getPrice()))).map(entity::setPrice);
        Optional.of(request.getAmount()).map(entity::setAmount);
        Optional.of(new BigDecimal(checkPrice(request.getPriceWithoutDiscount()))).map(entity::setPriceWithoutDiscount);
        ofNullable(request.getWeightInGrams()).map(entity::setWeightInGrams);
        ofNullable(request.getMilliliters()).map(entity::setMilliliters);
        ofNullable(request.getComposition()).map(entity::setComposition);

        ofNullable(request.getMadeIn()).map(entity::setMadeIn);
        ofNullable(request.getDetailOne()).map(entity::setDetailOne);
        ofNullable(request.getDetailTwo()).map(entity::setDetailTwo);
        ofNullable(request.getDetailThree()).map(entity::setDetailThree);
        entity.setUpdated(LocalDateTime.now());
    }

    @Transactional
    @Override
    public GoodsDto updateGoods(GoodsUpdateRequest request) {
        Goods entity = goodsRepository.findById(request.getId()).orElseThrow(
                () -> new GoodsEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        goodsUpdate(entity, request);
        return GoodsDto.toDto(goodsRepository.save(entity));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        Goods entity = goodsRepository.findById(id).orElseThrow(
                () -> new GoodsEntityNotFoundException(ERR_MESSAGE + id)
        );
        goodsRepository.delete(entity);
    }

    @Override
    public List<GoodsDto> findAllGoodsByCheckListId(Long checkListId) {
        CheckList checkList = checkListRepository.findById(checkListId).orElseThrow(
                () -> new CheckListEntityNotFoundException("CheckList Not Found with id=" + checkListId)
        );
        List<Goods> goodsList = goodsRepository.findGoodsByCheckList(checkList);
        return goodsList.stream()
                .map(GoodsDto::toDto)
                .collect(Collectors.toList());
    }
}
