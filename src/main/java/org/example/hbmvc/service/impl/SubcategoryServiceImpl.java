package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.prod.Category;
import org.example.hbmvc.domain.entity.prod.Subcategory;
import org.example.hbmvc.dto.subcategory.SubcategoryCreateRequest;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;
import org.example.hbmvc.dto.subcategory.SubcategoryFullDto;
import org.example.hbmvc.dto.subcategory.SubcategoryUpdateRequest;
import org.example.hbmvc.exception.CategoryEntityNotFoundException;
import org.example.hbmvc.exception.SubcategoryEntityNotFoundException;
import org.example.hbmvc.repository.CategoryRepository;
import org.example.hbmvc.repository.SubcategoryRepository;
import org.example.hbmvc.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class SubcategoryServiceImpl implements SubcategoryService {

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    @Override
    public SubcategoryDto create(SubcategoryCreateRequest request) {
        Subcategory subcategory = buildSubcategory(request);
        return SubcategoryDto.toDto(subcategoryRepository.save(subcategory));
    }

    private Subcategory buildSubcategory(SubcategoryCreateRequest request) {
        Category category = categoryRepository.findById(request.getCategoryId())
                .orElseThrow(
                        () -> new CategoryEntityNotFoundException("Category not found with id="
                                + request.getCategoryId()));
        Subcategory subcategory = new Subcategory();
        subcategory.setName(request.getName());
        subcategory.setDescription(request.getDescription());
        subcategory.setCategory(category);
//        subcategory.setCreated(LocalDateTime.now());
//        subcategory.setUpdated(LocalDateTime.now());
//        subcategory.setIsDeleted(Boolean.FALSE);

        return subcategory;
    }

    @Override
    public List<SubcategoryDto> findAll() {
        return subcategoryRepository.findAll().stream()
                .map(SubcategoryDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<SubcategoryDto> findAllByCategoryName(String categoryName) {
        Category category = categoryRepository.findCategoryByName(categoryName)
                .orElseThrow(() -> new CategoryEntityNotFoundException("Category not found with name="
                                + categoryName));
        List<Subcategory> subcategoryList = subcategoryRepository.findAllByCategory(category);
        return subcategoryList.stream()
                .map(SubcategoryDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<SubcategoryDto> findAllByCategoryId(Long categoryId) {
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CategoryEntityNotFoundException("Category not found with id=" + categoryId));
        List<Subcategory> subcategoryList = subcategoryRepository.findAllByCategory(category);
        return subcategoryList.stream()
                .map(SubcategoryDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public SubcategoryDto findByName(String name) {
        return SubcategoryDto.toDto(subcategoryRepository.findByName(name).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with name=" + name)
        ));
    }

    @Override
    public SubcategoryFullDto findById(Long id) {
        return SubcategoryFullDto.toDto(subcategoryRepository.findById(id).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with id=" + id)
        ));
    }

    @Transactional
    @Override
    public SubcategoryDto update(SubcategoryUpdateRequest request) {
        Subcategory subcategory = subcategoryRepository.findById(request.getId()).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with id=" + request.getId())
        );
        subcategoryUpdate(subcategory, request);
        return SubcategoryDto.toDto(subcategoryRepository.save(subcategory));
    }

    private void subcategoryUpdate(@NotNull Subcategory subcategory, @NotNull SubcategoryUpdateRequest request) {
        ofNullable(request.getName()).map(subcategory::setName);
        ofNullable(request.getDescription()).map(subcategory::setDescription);
  //      subcategory.setUpdated(LocalDateTime.now());
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        Subcategory subcategory = subcategoryRepository.findById(id).orElseThrow(
                () -> new SubcategoryEntityNotFoundException("Subcategory not found with id=" + id)
        );
        subcategoryRepository.delete(subcategory);
    }
}
