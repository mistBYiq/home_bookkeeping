package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.prod.Category;
import org.example.hbmvc.dto.category.CategoryCreateRequest;
import org.example.hbmvc.dto.category.CategoryDto;
import org.example.hbmvc.dto.category.CategoryFullDto;
import org.example.hbmvc.dto.category.CategoryUpdateRequest;
import org.example.hbmvc.exception.CategoryEntityNotFoundException;
import org.example.hbmvc.repository.CategoryRepository;
import org.example.hbmvc.repository.SubcategoryRepository;
import org.example.hbmvc.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDto create(CategoryCreateRequest request) {
        Category category = buildCategory(request);
        return CategoryDto.toDto(categoryRepository.save(category));
    }

    private Category buildCategory(CategoryCreateRequest request) {
        Category category = new Category();
        category.setName(request.getName());
        category.setDescription(request.getDescription());
//        category.setCreated(LocalDateTime.now());
//        category.setUpdated(LocalDateTime.now());
//        category.setIsDeleted(Boolean.FALSE);
        return category;
    }

    @Override
    public CategoryDto findByName(String name) {
        return CategoryDto.toDto(categoryRepository.findCategoryByName(name).orElseThrow(
                () -> new CategoryEntityNotFoundException("Category not found with name=" + name)
        ));
    }

    @Override
    public CategoryFullDto findCategoryFullDtoById(Long id) {
        return CategoryFullDto.toDto(categoryRepository.findById(id).orElseThrow(
                () -> new CategoryEntityNotFoundException("Category not found with id=" + id)
        ));
    }

    @Override
    public CategoryDto findCategoryDtoById(Long id) {
        return CategoryDto.toDto(categoryRepository.findById(id).orElseThrow(
                () -> new CategoryEntityNotFoundException("Category not found with id=" + id)
        ));
    }

    @Transactional
    @Override
    public CategoryFullDto update(CategoryUpdateRequest request) {
        Category category = categoryRepository.findById(request.getId()).orElseThrow(
                () -> new CategoryEntityNotFoundException("Category not found with id=" + request.getId())
        );
        categoryUpdate(category, request);
        return CategoryFullDto.toDto(category);
    }

    private void categoryUpdate(@NotNull Category category, @NotNull CategoryUpdateRequest request) {
        ofNullable(request.getName()).map(category::setName);
        ofNullable(request.getDescription()).map(category::setDescription);
//        category.setUpdated(LocalDateTime.now());
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(
                () -> new CategoryEntityNotFoundException("Category not found with id=" + id)
        );
        categoryRepository.delete(category);
    }
}
