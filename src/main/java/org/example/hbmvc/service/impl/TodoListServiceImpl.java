package org.example.hbmvc.service.impl;

import org.example.hbmvc.domain.entity.plan.TodoList;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.todolist.TodoListCreateRequest;
import org.example.hbmvc.dto.todolist.TodoListUpdateRequest;
import org.example.hbmvc.dto.todolist.TodoListDto;
import org.example.hbmvc.dto.todolist.TodoListFullDto;
import org.example.hbmvc.exception.TodoListEntityNotFoundException;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.TodoListRepository;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class TodoListServiceImpl implements TodoListService {

    private static final String ERR_MESSAGE = "TodoList not found with id =";

    @Autowired
    private TodoListRepository todoListRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<TodoListDto> findAllFavoritesByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + userId)
        );
        return todoListRepository.findAllByUserAndIsFavouritesIsTrue(user)
                .stream()
                .map(TodoListDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<TodoListDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + userId)
        );
        return todoListRepository.findAllByUser(user)
                .stream()
                .map(TodoListDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public TodoListFullDto create(TodoListCreateRequest request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + request.getUserId())
        );
        TodoList todoList = buildTodoList(request, user);
        return TodoListFullDto.toDto(todoListRepository.save(todoList));
    }

    private TodoList buildTodoList(TodoListCreateRequest request, User user) {
        return new TodoList()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setUser(user)
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE)
                .setIsFavourites(Boolean.FALSE)
                .setIsDone(Boolean.FALSE);
    }

    @Transactional
    @Override
    public TodoListFullDto update(TodoListUpdateRequest request) {
        TodoList todoList = todoListRepository.findById(request.getId()).orElseThrow(
                () -> new TodoListEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        todoListUpdate(todoList, request);
        return TodoListFullDto.toDto(todoListRepository.save(todoList));
    }

    private void todoListUpdate(TodoList todoList, TodoListUpdateRequest request) {
        ofNullable(request.getName()).map(todoList::setName);
        ofNullable(request.getDescription()).map(todoList::setDescription);
        ofNullable(request.getIsDone()).map(todoList::setIsDone);
        ofNullable(request.getIsFavorites()).map(todoList::setIsFavourites);

        todoList.setUpdated(LocalDateTime.now());
    }

    @Override
    public TodoListDto findTodoListById(Long id) {
        return TodoListDto.toDto(todoListRepository.findById(id).orElseThrow(
                () -> new TodoListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public TodoListFullDto findTodoListFullById(Long id) {
        return TodoListFullDto.toDto(todoListRepository.findById(id).orElseThrow(
                () -> new TodoListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        TodoList todoList = todoListRepository.findById(id).orElseThrow(
                () -> new TodoListEntityNotFoundException(ERR_MESSAGE + id)
        );
        todoListRepository.delete(todoList);
    }
}
