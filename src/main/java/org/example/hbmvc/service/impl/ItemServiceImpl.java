package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.domain.entity.plan.Item;
import org.example.hbmvc.domain.enums.Importance;
import org.example.hbmvc.dto.item.ItemCreateRequest;
import org.example.hbmvc.dto.item.ItemDto;
import org.example.hbmvc.dto.item.ItemUpdateRequest;
import org.example.hbmvc.exception.BuyListEntityNotFoundException;
import org.example.hbmvc.exception.ItemEntityNotFoundException;
import org.example.hbmvc.repository.BuyListRepository;
import org.example.hbmvc.repository.ItemRepository;
import org.example.hbmvc.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class ItemServiceImpl implements ItemService {

    private static final String ERR_MESSAGE = "Item not found with id =";

    private final ItemRepository itemRepository;
    private final BuyListRepository buyListRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository,
                           BuyListRepository buyListRepository) {
        this.itemRepository = itemRepository;
        this.buyListRepository = buyListRepository;
    }

    @Override
    public List<Item> findAll() {
        return itemRepository.findAll();
    }


    @Override
    public Page<Item> findPaginated(int pageNo,
                                    int pageSize,
                                    String sortField,
                                    String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.itemRepository.findAll(pageable);
    }

    @Override
    public List<ItemDto> findAllItems() {
        return itemRepository.findAll()
                .stream()
                .map(ItemDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ItemDto create(ItemCreateRequest request) {
        Item item = buildItem(request);
        return ItemDto.toDto(itemRepository.save(item));
    }

    @Transactional
    @Override
    public ItemDto create(ItemCreateRequest request, Long buyListId) {
        Item item = buildItem(request);
        BuyList buyList = buyListRepository.findById(buyListId).orElseThrow(
                () -> new BuyListEntityNotFoundException("BuyList not found with id =" + buyListId)
        );
        item.setBuyList(buyList);
        return ItemDto.toDto(itemRepository.save(item));
    }

    @Transactional
    @Override
    public ItemDto update(ItemUpdateRequest request) {
        Item item = itemRepository.findById(request.getId()).orElseThrow(
                () -> new ItemEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        itemUpdate(item, request);
        return ItemDto.toDto(itemRepository.save(item));
    }

    @Override
    public ItemDto findById(Long id) {
        return ItemDto.toDto(itemRepository.findById(id).orElseThrow(
                () -> new ItemEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        itemRepository.findById(id).orElseThrow(
                () -> new ItemEntityNotFoundException(ERR_MESSAGE + id)
        );
        itemRepository.deleteById(id);
    }

    private Item buildItem(ItemCreateRequest request) {
        BuyList buyList = buyListRepository.findById(request.getBuyListId()).orElseThrow(
                () -> new BuyListEntityNotFoundException("BuyList not found with id =" + request.getBuyListId())
        );
        Item item = new Item();
        item.setName(request.getName());
        item.setPrice(request.getPrice());
        item.setCreated(LocalDateTime.now());
        item.setUpdated(LocalDateTime.now());
        item.setIsDeleted(Boolean.FALSE);
        item.setBuyList(buyList);
        item.setDescription(request.getDescription());
        item.setAmount(Math.max(request.getAmount(), 1));
        if (request.getImportance() != null) {
            item.setImportance(request.getImportance());
        } else {
            item.setImportance(Importance.NORMAL);
        }

        return item;
    }

    private void itemUpdate(@NotNull Item item, @NotNull ItemUpdateRequest request) {
        ofNullable(request.getName()).map(item::setName);
        ofNullable(request.getDescription()).map(item::setDescription);
        ofNullable(request.getPrice()).map(item::setPrice);
        Optional.of(request.getAmount()).map(item::setAmount);
        ofNullable(request.getImportance()).map(item::setImportance);
    }
}
