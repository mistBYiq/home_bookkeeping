package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.domain.entity.check.Goods;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.checklist.CheckListCreateRequest;
import org.example.hbmvc.dto.checklist.CheckListDto;
import org.example.hbmvc.dto.checklist.CheckListFullDto;
import org.example.hbmvc.dto.checklist.CheckListUpdateRequest;
import org.example.hbmvc.exception.CheckListEntityNotFoundException;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.CheckListRepository;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.CheckListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class CheckListServiceImpl implements CheckListService {

    private static final String ERR_MESSAGE = "CheckList not found with id =";
    private static final String DATE_FORMAT = "dd.MM.yyyy";

    @Autowired
    private CheckListRepository checkListRepository;

    @Autowired
    private UserRepository userRepository;

    //  private final LinkProductCheckListRepository productCheckListRepository;

    @Override
    public List<CheckList> findAll() {
        return checkListRepository.findAll();
    }

    @Override
    public Page<CheckList> findPaginated(int pageNo,
                                         int pageSize,
                                         String sortField,
                                         String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.checkListRepository.findAll(pageable);
    }


    @Override
    public CheckListDto findCheckListById(Long id) {
        return CheckListDto.toDto(checkListRepository.findById(id).orElseThrow(
                () -> new CheckListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public List<CheckListDto> findAllByUserEmail(String email) {

        return checkListRepository.findByUserEmail(email)
                .stream()
                .map(CheckListDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public CheckListFullDto findCheckListFullById(Long id) {
        return CheckListFullDto.toDto(checkListRepository.findById(id).orElseThrow(
                () -> new CheckListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public List<CheckListDto> findAllChecks() {
        return checkListRepository.findAll()
                .stream()
                .map(CheckListDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CheckListDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + userId)
        );
        return checkListRepository.findAllByUser(user)
                .stream()
                .map(CheckListDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public CheckListFullDto create(CheckListCreateRequest request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + request.getUserId()));
        CheckList checkList = buildCheckList(request, user);
        return CheckListFullDto.toDto(checkListRepository.save(checkList));
    }

    @Transactional
    @Override
    public CheckListFullDto update(CheckListUpdateRequest request) {
        CheckList checkList = checkListRepository.findById(request.getId()).orElseThrow(
                () -> new CheckListEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        checkListUpdate(checkList, request);
        return CheckListFullDto.toDto(checkListRepository.save(checkList));
    }


    @Transactional
    @Override
    public void deleteById(Long id) {
        CheckList checkList = checkListRepository.findById(id).orElseThrow(
                () -> new CheckListEntityNotFoundException(ERR_MESSAGE + id)
        );
//        Iterator<Goods> iterator = checkList.getGoodsList().iterator();
//        while(iterator.hasNext()){
//          //  Goods goods = iterator.next();
//            iterator.remove();
//        }

        // ??? ConcurrentModificationException
//        for (Goods goods : checkList.getGoodsList()) {
//            checkList.removeGoods(goods);
//        }

        for (int i = 0; i < checkList.getGoodsList().size(); i++) {
            Goods goods = checkList.getGoodsList().get(i);
            checkList.removeGoods(goods);
        }
        checkListRepository.delete(checkList);
    }

    @Override
    public double countAllTotalPriceByUserId(Long id) {
        return checkListRepository.countAllTotalPriceByUserId(id);
    }

    @Override
    public double countSumPerMonthByUserId(Long id) {
        return checkListRepository.countSumPerMonthByUserId(id);
    }

    @Override
    public double countSumPerYearByUserId(Long id) {
        return checkListRepository.countSumPerYearByUserId(id);
    }

    @Override
    public double countSumPerWeekByUserId(Long id) {
        return checkListRepository.countSumPerWeekByUserId(id);
    }

    private CheckList buildCheckList(CheckListCreateRequest request, User user) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        String strDate = checkSlash(request.getDate());
        String strPrice = checkComma(request.getTotalPrice());

        return new CheckList()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setPlace(request.getPlace())
                .setUser(user)
                .setPurchaseDate(LocalDate.parse((strDate), formatter))
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE)
                .setDiscount(BigDecimal.ZERO)
                // todo
                .setTotalPrice(new BigDecimal(strPrice));
    }

    private String checkSlash(String str) {
        if (str.contains("/")) {
            return str.replace("/", ".");
        }
        return str;
    }

    private String checkComma(String str) {
        if (str.contains(",")) {
            return str.replaceFirst(",", ".");
        }
        return str;
    }

    private void checkListUpdate(@NotNull CheckList checkList, @NotNull CheckListUpdateRequest request) {
        ofNullable(request.getName()).map(checkList::setName);
        ofNullable(request.getDescription()).map(checkList::setDescription);
        ofNullable(request.getPlace()).map(checkList::setPlace);
        checkList.setUpdated(LocalDateTime.now());
        //checkList.setTotalPrice(countSum(checkList.getProducts()));
    }

//    private BigDecimal countSum(List<Goods> goods) {
//        BigDecimal result = 0;
//        for (Goods goods : goods) {
//            result += goods.getPrice();
//        }
//        return result;
//    }
}
