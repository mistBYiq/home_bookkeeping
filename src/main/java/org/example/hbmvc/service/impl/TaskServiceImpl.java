package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.plan.Task;
import org.example.hbmvc.domain.entity.plan.TodoList;
import org.example.hbmvc.domain.enums.TaskStatus;
import org.example.hbmvc.dto.task.TaskCreateRequest;
import org.example.hbmvc.dto.task.TaskDto;
import org.example.hbmvc.dto.task.TaskUpdateRequest;
import org.example.hbmvc.exception.TaskEntityNotFoundException;
import org.example.hbmvc.exception.TodoListEntityNotFoundException;
import org.example.hbmvc.repository.TaskRepository;
import org.example.hbmvc.repository.TodoListRepository;
import org.example.hbmvc.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static java.util.Optional.ofNullable;

@Service
public class TaskServiceImpl implements TaskService {

    private static final String ERR_MESSAGE = "Todo not found with id =";

    private final TaskRepository taskRepository;
    private final TodoListRepository todoListRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, TodoListRepository todoListRepository) {
        this.taskRepository = taskRepository;
        this.todoListRepository = todoListRepository;
    }


    @Transactional
    @Override
    public TaskDto create(TaskCreateRequest request) {
        Task task = buildTodo(request);
        return TaskDto.toDto(taskRepository.save(task));
    }

    @Transactional
    @Override
    public TaskDto update(TaskUpdateRequest request) {
        Task task = taskRepository.findById(request.getId()).orElseThrow(
                () -> new TaskEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        todoUpdate(task, request);
        return TaskDto.toDto(taskRepository.save(task));
    }

    @Override
    public TaskDto findById(Long id) {
        return TaskDto.toDto(taskRepository.findById(id).orElseThrow(
                () -> new TaskEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        taskRepository.findById(id).orElseThrow(
                () -> new TaskEntityNotFoundException(ERR_MESSAGE + id)
        );
        taskRepository.deleteById(id);
    }

    private Task buildTodo(TaskCreateRequest request) {
        // service ??
        TodoList todoList = todoListRepository.findById(request.getTodoListId()).orElseThrow(
                () -> new TodoListEntityNotFoundException("TodoList not found with id =" + request.getTodoListId())
        );
        Task task = new Task();
        task.setName(request.getName());
        task.setDescription(request.getDescription());
        task.setTodoList(todoList);

        task.setPlanTime(LocalDateTime.now());

        task.setCreated(LocalDateTime.now());
        task.setUpdated(LocalDateTime.now());
        task.setIsDeleted(Boolean.FALSE);
        task.setStatus(TaskStatus.NOT_SELECTED);
        task.setIsDone(Boolean.FALSE);

        return task;
    }

    private void todoUpdate(@NotNull Task task, @NotNull TaskUpdateRequest request) {
        ofNullable(request.getName()).map(task::setName);
        ofNullable(request.getDescription()).map(task::setDescription);
        ofNullable(request.getStatus()).map(task::setStatus);
        //   private LocalDateTime planTime;
        ofNullable(request.getStatus()).map(task::setStatus);
        ofNullable(request.getIsDone()).map(task::setIsDone);
        task.setUpdated(LocalDateTime.now());
    }
}
