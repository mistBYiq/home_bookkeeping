package org.example.hbmvc.service.impl;

import com.sun.istack.NotNull;
import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.domain.entity.fin.Report;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.income.IncomeFullDto;
import org.example.hbmvc.dto.income.IncomeUpdateRequest;
import org.example.hbmvc.dto.report.ReportCreateRequest;
import org.example.hbmvc.dto.report.ReportDto;
import org.example.hbmvc.dto.report.ReportUpdateRequest;
import org.example.hbmvc.exception.ReportEntityNotFoundException;
import org.example.hbmvc.exception.UserEntityNotFoundException;
import org.example.hbmvc.repository.ReportRepository;
import org.example.hbmvc.repository.UserRepository;
import org.example.hbmvc.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class ReportServiceImpl implements ReportService {

    private static final String ERR_MESSAGE = "Report not found with id =";

    private final ReportRepository reportRepository;
    private final UserRepository userRepository;

    @Autowired
    public ReportServiceImpl(ReportRepository reportRepository, UserRepository userRepository) {
        this.reportRepository = reportRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ReportDto> findAllByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserEntityNotFoundException("ReportService. User not found with id=" + userId)
        );
        List<Report> reports = reportRepository.findAllByUser(user);

        return reports.stream()
                .map(ReportDto::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReportDto> findAllReports() {
        return reportRepository.findAll()
                .stream()
                .map(ReportDto::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ReportDto create(ReportCreateRequest request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("Report Service. User not found with id=" + request.getUserId())
        );
        Report report = buildReport(request, user);
        return ReportDto.toDto(reportRepository.save(report));
    }

    private Report buildReport(ReportCreateRequest request, User user) {

        return new Report()
                .setName(request.getName())
                .setTimeline(request.getTimeline())
                .setUser(user)
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE);
    }

    @Transactional
    @Override
    public ReportDto update(ReportUpdateRequest request) {
        Report report = reportRepository.findById(request.getId()).orElseThrow(
                () -> new ReportEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        reportUpdate(report, request);
        return ReportDto.toDto(reportRepository.save(report));
    }

    private void reportUpdate(@NotNull Report entity, @NotNull ReportUpdateRequest request) {
        ofNullable(request.getName()).map(entity::setName);
        ofNullable(request.getTimeline()).map(entity::setTimeline);

        entity.setUpdated(LocalDateTime.now());
    }

    @Override
    public ReportDto findReportById(Long id) {
        return ReportDto.toDto(reportRepository.findById(id).orElseThrow(
                () -> new ReportEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public void deleteById(Long id) {
        Report report = reportRepository.findById(id).orElseThrow(
                () -> new ReportEntityNotFoundException(ERR_MESSAGE + id)
        );
        reportRepository.delete(report);
    }
}
