package org.example.hbmvc.service;

import org.example.hbmvc.dto.subcategory.SubcategoryCreateRequest;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;
import org.example.hbmvc.dto.subcategory.SubcategoryFullDto;
import org.example.hbmvc.dto.subcategory.SubcategoryUpdateRequest;

import java.util.List;

public interface SubcategoryService {

    SubcategoryDto create(SubcategoryCreateRequest createRequest);

    List<SubcategoryDto> findAll();

    List<SubcategoryDto> findAllByCategoryName(String categoryName);

    List<SubcategoryDto> findAllByCategoryId(Long categoryId);

    SubcategoryDto findByName(String name);

    SubcategoryFullDto findById(Long id);

    SubcategoryDto update(SubcategoryUpdateRequest updateRequest);

    void deleteById(Long id);

}
