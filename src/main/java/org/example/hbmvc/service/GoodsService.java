package org.example.hbmvc.service;


import org.example.hbmvc.dto.goods.GoodsCreateRequest;
import org.example.hbmvc.dto.goods.GoodsDto;
import org.example.hbmvc.dto.goods.GoodsFullDto;
import org.example.hbmvc.dto.goods.GoodsUpdateRequest;

import java.util.List;

public interface GoodsService {
    GoodsDto createGoods(GoodsCreateRequest request);
    void create(GoodsCreateRequest request);
    GoodsFullDto findGoodsFullDtoById(Long id);
    GoodsDto findById(Long id);
    void update(GoodsUpdateRequest request);
    GoodsDto updateGoods(GoodsUpdateRequest request);
    void deleteById(Long id);
    List<GoodsDto> findAllGoodsByCheckListId(Long checkListId);

}
