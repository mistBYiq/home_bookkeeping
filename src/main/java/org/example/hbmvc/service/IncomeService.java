package org.example.hbmvc.service;

import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.dto.income.IncomeCreateRequest;
import org.example.hbmvc.dto.income.IncomeDto;
import org.example.hbmvc.dto.income.IncomeFullDto;
import org.example.hbmvc.dto.income.IncomeUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IncomeService {
    List<IncomeDto> findAllByUserId(Long userId);

    List<Income> findAll();

    Page<Income> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    List<IncomeDto> findAllIncomes();

    IncomeFullDto create(IncomeCreateRequest request);

    IncomeFullDto update(IncomeUpdateRequest request);

    IncomeFullDto findIncomeById(Long id);

    void deleteById(Long id);

    double countAllTotalSumByUserId(Long id);
}
