package org.example.hbmvc.service;

import org.example.hbmvc.dto.task.TaskCreateRequest;
import org.example.hbmvc.dto.task.TaskUpdateRequest;
import org.example.hbmvc.dto.task.TaskDto;

public interface TaskService {

    TaskDto create(TaskCreateRequest request);

    TaskDto update(TaskUpdateRequest request);

    TaskDto findById(Long id);

    void deleteById(Long id);
}
