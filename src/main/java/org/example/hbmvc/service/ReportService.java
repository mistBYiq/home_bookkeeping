package org.example.hbmvc.service;

import org.example.hbmvc.dto.report.ReportCreateRequest;
import org.example.hbmvc.dto.report.ReportDto;
import org.example.hbmvc.dto.report.ReportUpdateRequest;

import java.util.List;

public interface ReportService {
    List<ReportDto> findAllByUserId(Long userId);

    List<ReportDto> findAllReports();

    ReportDto create(ReportCreateRequest request);

    ReportDto update(ReportUpdateRequest request);

    ReportDto findReportById(Long id);

    void deleteById(Long id);
}
