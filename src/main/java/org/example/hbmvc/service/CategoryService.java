package org.example.hbmvc.service;

import org.example.hbmvc.dto.category.CategoryCreateRequest;
import org.example.hbmvc.dto.category.CategoryDto;
import org.example.hbmvc.dto.category.CategoryFullDto;
import org.example.hbmvc.dto.category.CategoryUpdateRequest;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> findAll();

    CategoryDto create(CategoryCreateRequest createRequest);

    CategoryDto findByName(String name);

    CategoryFullDto findCategoryFullDtoById(Long id);

    CategoryDto findCategoryDtoById(Long id);

    CategoryFullDto update(CategoryUpdateRequest updateRequest);

    void deleteById(Long id);
}
