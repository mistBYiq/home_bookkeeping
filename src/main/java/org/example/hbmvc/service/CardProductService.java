package org.example.hbmvc.service;

import org.example.hbmvc.dto.cardproduct.CardProductCreateRequest;
import org.example.hbmvc.dto.cardproduct.CardProductDto;
import org.example.hbmvc.dto.cardproduct.CardProductFullDto;
import org.example.hbmvc.dto.cardproduct.CardProductUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CardProductService {
    CardProductDto createCardProduct(CardProductCreateRequest request);

    void create(CardProductCreateRequest request);

    CardProductFullDto findProductFullDtoById(Long id);

    CardProductDto findProductDtoById(Long id);

    CardProductDto findByName(String name);

    void update(CardProductUpdateRequest request);

    CardProductDto updateCardProduct(CardProductUpdateRequest request);

    void deleteById(Long id);

    void deleteByName(String name);

    List<CardProductDto> findAll();

    List<CardProductDto> findAllByNameLike(String query);

    Page<CardProductDto> findAllByNameLikePaging(String query,
                                                 int pageNo,
                                                 int pageSize,
                                                 String sortField,
                                                 String sortDirection);

}
