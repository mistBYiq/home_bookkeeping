package org.example.hbmvc.service;

import org.example.hbmvc.dto.todolist.TodoListCreateRequest;
import org.example.hbmvc.dto.todolist.TodoListDto;
import org.example.hbmvc.dto.todolist.TodoListFullDto;
import org.example.hbmvc.dto.todolist.TodoListUpdateRequest;

import java.util.List;

public interface TodoListService {
    TodoListFullDto create(TodoListCreateRequest request);

    TodoListFullDto update(TodoListUpdateRequest request);

    TodoListDto findTodoListById(Long id);

    TodoListFullDto findTodoListFullById(Long id);

    void deleteById(Long id);

    List<TodoListDto> findAllFavoritesByUserId(Long userId);

    List<TodoListDto> findAllByUserId(Long userId);
}
