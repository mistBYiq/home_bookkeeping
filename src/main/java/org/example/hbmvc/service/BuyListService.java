package org.example.hbmvc.service;

import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.dto.buylist.BuyListCreateRequest;
import org.example.hbmvc.dto.buylist.BuyListFullDto;
import org.example.hbmvc.dto.buylist.BuyListUpdateRequest;
import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.checklist.CheckListDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BuyListService {
    List<BuyList> findAll();

    Page<BuyList> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    List<BuyListDto> findAllLists();

    List<BuyListDto> findAllFavoritesByUserId(Long userId);

    List<BuyListDto> findAllByUserId(Long userId);

    BuyListFullDto create(BuyListCreateRequest buyList);

    BuyListFullDto update(BuyListUpdateRequest request);

    BuyListDto findBuyListById(Long id);

    BuyListFullDto findBuyListFullById(Long id);

    void deleteById(Long id);
}
