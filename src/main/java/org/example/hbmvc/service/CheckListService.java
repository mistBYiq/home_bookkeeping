package org.example.hbmvc.service;

import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.dto.checklist.CheckListCreateRequest;
import org.example.hbmvc.dto.checklist.CheckListDto;
import org.example.hbmvc.dto.checklist.CheckListFullDto;
import org.example.hbmvc.dto.checklist.CheckListUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CheckListService {
    List<CheckListDto> findAllByUserId(Long userId);
    List<CheckListDto> findAllByUserEmail(String email);

    List<CheckList> findAll();

    Page<CheckList> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    List<CheckListDto> findAllChecks();

    CheckListFullDto create(CheckListCreateRequest checkList);

    CheckListFullDto update(CheckListUpdateRequest request);

    CheckListDto findCheckListById(Long id);

    CheckListFullDto findCheckListFullById(Long id);

    void deleteById(Long id);

    double countAllTotalPriceByUserId(Long id);

    double countSumPerMonthByUserId(Long id);

    double countSumPerYearByUserId(Long id);

    double countSumPerWeekByUserId(Long id);
}
