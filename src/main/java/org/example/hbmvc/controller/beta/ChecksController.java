package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.checklist.CheckListDto;
import org.example.hbmvc.service.CheckListService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/checks")
public class ChecksController {
    private final CheckListService service;

    public ChecksController(CheckListService service) {
        this.service = service;
    }

   // @Secured()
    @GetMapping("/all")
    public String findAll(Model model) {
        List<CheckListDto> checks = service.findAllChecks();
        model.addAttribute("checks", checks);
        return "check/checks";
    }

    @GetMapping("/")
    public String findAllUserChecks(Model model,
                                    Principal principal) {
        List<CheckListDto> checks = service.findAllByUserEmail(principal.getName());
        model.addAttribute("checks", checks);
        return "check/checks";
    }

    @GetMapping("/{id}")
    public String findAllUserChecks(@PathVariable("id") Long id,
                                    Model model,
                                    Principal principal) {
        CheckListDto check = service.findCheckListById(id);
        model.addAttribute("check", check);
        return "check/check_page";
    }
}
