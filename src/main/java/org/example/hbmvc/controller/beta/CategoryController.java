package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.category.CategoryCreateRequest;
import org.example.hbmvc.dto.category.CategoryDto;
import org.example.hbmvc.dto.category.CategoryFullDto;
import org.example.hbmvc.dto.category.CategoryUpdateRequest;
import org.example.hbmvc.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/")
    public String findAllCategories(Model model) {
        List<CategoryDto> categoryDtoList = categoryService.findAll();
        model.addAttribute("categoryDtoList", categoryDtoList);
        return "category/categories";
    }

    @GetMapping("/createForm")
    public String showCreateCategoryForm(Model model) {
        CategoryCreateRequest createRequest = new CategoryCreateRequest();

        model.addAttribute("createRequest", createRequest);
        return "category/create_category";
    }

    @PostMapping("/create")
    public String createCategory(@ModelAttribute("createRequest") CategoryCreateRequest createRequest) {
        categoryService.create(createRequest);

        return "redirect:/categories/";
    }

    @GetMapping("/{id}")
    public String findCategoryById(@PathVariable("id") Long id,
                                   Model model) {
        CategoryFullDto categoryDto = categoryService.findCategoryFullDtoById(id);
        model.addAttribute("categoryDto", categoryDto);
        return "category/category_page";
    }

    @GetMapping("/{id}/updateForm")
    public String showUpdateCategoryFormById(@PathVariable(value = "id") Long categoryId,
                                             Model model) {
        CategoryDto categoryDto = categoryService.findCategoryDtoById(categoryId);
        CategoryUpdateRequest updateRequest = new CategoryUpdateRequest();
        model.addAttribute("categoryDto", categoryDto);
        model.addAttribute("updateRequest", updateRequest);

        return "category/update_category";
    }

    @PostMapping("/update")
    public String updateCategory(@ModelAttribute("updateRequest") CategoryUpdateRequest updateRequest) {
        categoryService.update(updateRequest);
        return "redirect:/categories/";
    }

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable(value = "id") Long categoryId) {
        this.categoryService.deleteById(categoryId);
        return "redirect:/categories/";
    }
}
