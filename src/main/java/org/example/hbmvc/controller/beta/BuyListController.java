package org.example.hbmvc.controller.beta;

import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.dto.buylist.BuyListCreateRequest;
import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.buylist.BuyListFullDto;
import org.example.hbmvc.dto.buylist.BuyListUpdateRequest;
import org.example.hbmvc.service.BuyListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/users/")
public class BuyListController {

    @Autowired
    private BuyListService buyListService;

    @GetMapping("/test/{userId}/buylists/")
    public String viewBuyLists(Model model) {
        return findUserBuyLists(1, "Name", "asc", model);
    }

    @GetMapping("/test/{userId}/buylists/page/{pageNo}")
    public String findUserBuyLists(@PathVariable(value = "pageNo") int pageNo,
                                   @RequestParam("sortField") String sortField,
                                   @RequestParam("sortDir") String sortDir,
                                   Model model) {
        int pageSize = 5;

        Page<BuyList> page = buyListService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<BuyList> buyLists = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("buyLists", buyLists);
        return "/buylist/buylists";
    }

    @GetMapping("/{userId}/buylists")
    public String findAllBuylistsForUser(@PathVariable("userId") Long userId,
                                         Model model) {
        List<BuyListDto> buyListDtoList = buyListService.findAllByUserId(userId);
        model.addAttribute("buyListDtoList", buyListDtoList);
        model.addAttribute("userId", userId);
        return "buylist/buylists";
    }

    @GetMapping("/{userId}/buylists/createForm")
    public String showCreateBuyListForm(@PathVariable("userId") Long userId,
                                        Model model) {
        BuyListCreateRequest createRequest = new BuyListCreateRequest();
        createRequest.setUserId(userId);
        model.addAttribute("createRequest", createRequest);
        return "buylist/create_buylist";
    }

    @PostMapping("/buylists/create")
    public String createBuyList(@ModelAttribute("createRequest") BuyListCreateRequest createRequest,
                                Model model) {
        BuyListFullDto buyListDto = buyListService.create(createRequest);
        model.addAttribute("buyListDto", buyListDto);
        return "buylist/buylist_page";
    }

    @GetMapping("/buylists/{id}")
    public String findBuyListById(@PathVariable("id") Long buyListId,
                                  Model model) {
        BuyListFullDto buyListDto = buyListService.findBuyListFullById(buyListId);
        model.addAttribute("buyListDto", buyListDto);
        return "buylist/buylist_page";
    }

    @GetMapping("/buylists/{id}/updateForm")
    public String showUpdateBuyListFormById(@PathVariable(value = "id") Long buyListId,
                                            Model model) {

        BuyListDto buyListDto = buyListService.findBuyListById(buyListId);
        BuyListUpdateRequest updateRequest = new BuyListUpdateRequest();
        model.addAttribute("buyListDto", buyListDto);
        model.addAttribute("updateRequest", updateRequest);

        return "buylist/update_buylist";
    }

    @PostMapping("/buylists/update")
    public String updateBuyList(@ModelAttribute("updateRequest") BuyListUpdateRequest updateRequest,
                                Model model) {
        BuyListFullDto buyListDto = buyListService.update(updateRequest);
        model.addAttribute("buyListDto", buyListDto);
        return "buylist/buylist_page";
    }

    @GetMapping("/{userId}/buylists/{id}/delete")
    public String deleteBuyList(@PathVariable(value = "userId") Long userId,
                                @PathVariable(value = "id") Long buyListId) {

        this.buyListService.deleteById(buyListId);
        return "redirect:/users/" + userId;
    }
}