package org.example.hbmvc.controller.beta;

import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.dto.income.IncomeCreateRequest;
import org.example.hbmvc.dto.income.IncomeDto;
import org.example.hbmvc.dto.income.IncomeFullDto;
import org.example.hbmvc.dto.income.IncomeUpdateRequest;
import org.example.hbmvc.service.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users/")
public class IncomeController {

    @Autowired
    private IncomeService incomeService;


    @GetMapping("/notest/{userId}/incomes/")
    public String viewIncomes(@PathVariable("userId") Long userId,
                              Model model) {
        return findUserIncomes(1, "Name", "asc", model, userId);
    }

    @GetMapping("/notest/{userId}/incomes/page/{pageNo}")
    public String findUserIncomes(@PathVariable(value = "pageNo") int pageNo,
                                  @RequestParam("sortField") String sortField,
                                  @RequestParam("sortDir") String sortDir,
                                  Model model,
                                  @PathVariable("userId") Long userId) {
        int pageSize = 5;

        Page<Income> page = incomeService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<Income> incomes = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("incomes", incomes);
        model.addAttribute("userId", userId);
        return "/income/incomes";
    }

    @GetMapping("/{userId}/incomes")
    public String getAllUserIncomes(@PathVariable("userId") Long userId,
                                    Model model) {
        List<IncomeDto> incomeDtoList = incomeService.findAllByUserId(userId);
        model.addAttribute("incomeDtoList", incomeDtoList);
        model.addAttribute("userId", userId);
        return "income/incomes";
    }

    @GetMapping("/{userId}/incomes/createForm")
    public String showCreateIncomeForm(@PathVariable("userId") Long userId,
                                       Model model) {
        IncomeCreateRequest createRequest = new IncomeCreateRequest();
        createRequest.setUserId(userId);
        model.addAttribute("createRequest", createRequest);
        return "income/create_income";
    }

    @PostMapping("/incomes/create")
    public String createIncome(@ModelAttribute("createRequest") @Valid IncomeCreateRequest createRequest,
                               BindingResult result,
                               Model model) {
        if(result.hasErrors()) {
            model.addAttribute("createRequest", createRequest);
            return "income/create_income";
        }
        IncomeFullDto incomeDto = incomeService.create(createRequest);
        model.addAttribute("incomeDto", incomeDto);
        return "income/income_page";
    }


    @GetMapping("/incomes/{id}")
    public String findIncomeById(@PathVariable("id") Long incomeId,
                                 Model model) {
        IncomeFullDto incomeDto = incomeService.findIncomeById(incomeId);
        model.addAttribute("incomeDto", incomeDto);
        return "income/income_page";
    }

    @GetMapping("/incomes/{id}/updateForm")
    public String showUpdateIncomeFormById(@PathVariable(value = "id") Long incomeId,
                                           Model model) {
        IncomeFullDto incomeDto = incomeService.findIncomeById(incomeId);
        IncomeUpdateRequest updateRequest = new IncomeUpdateRequest();
        model.addAttribute("incomeDto", incomeDto);
        model.addAttribute("updateRequest", updateRequest);

        return "income/update_income";
    }

    @PostMapping("/incomes/update")
    public String updateBuyList(@ModelAttribute("updateRequest") IncomeUpdateRequest updateRequest,
                                Model model) {
        IncomeFullDto incomeDto = incomeService.update(updateRequest);
        model.addAttribute("incomeDto", incomeDto);
        return "income/income_page";
    }

    @GetMapping("/{userId}/incomes/{id}/delete")
    public String deleteBuyList(@PathVariable(value = "userId") Long userId,
                                @PathVariable(value = "id") Long incomeId) {

        this.incomeService.deleteById(incomeId);
        return "redirect:/users/" + userId + "/incomes";
    }

}