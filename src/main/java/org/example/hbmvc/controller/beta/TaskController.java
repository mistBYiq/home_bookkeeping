package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.task.TaskCreateRequest;
import org.example.hbmvc.dto.task.TaskDto;
import org.example.hbmvc.dto.task.TaskUpdateRequest;
import org.example.hbmvc.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/users/todolists/")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/{todoListId}/tasks/createForm")
    public String showCreateTaskForm(@PathVariable("todoListId") Long taskListId,
                                     Model model) {
        TaskCreateRequest createRequest = new TaskCreateRequest();
        createRequest.setTodoListId(taskListId);
        model.addAttribute("createRequest", createRequest);
        return "task/create_task";
    }

    @PostMapping("/tasks/create")
    public String createTask(@ModelAttribute("createRequest") TaskCreateRequest createRequest) {
        taskService.create(createRequest);
        return "redirect:/users/todolists/" + createRequest.getTodoListId();
    }

    @GetMapping("/tasks/{id}")
    public String findTaskById(@PathVariable("id") Long id,
                               Model model) {
        TaskDto taskDto = taskService.findById(id);
        model.addAttribute("taskDto", taskDto);
        return "task/task_page";
    }

    @GetMapping("/tasks/{id}/updateForm")
    public String showUpdateTaskForm(@PathVariable("id") Long id,
                                     Model model) {
        TaskDto taskDto = taskService.findById(id);
        TaskUpdateRequest updateRequest = new TaskUpdateRequest();
        model.addAttribute("taskDto", taskDto);
        model.addAttribute("updateRequest", updateRequest);
        return "/task/update_task";
    }

    @PostMapping("/tasks/update")
    public String updateTask(@ModelAttribute("updateRequest") TaskUpdateRequest updateRequest,
                             Model model) {
        TaskDto taskDto = taskService.update(updateRequest);
        model.addAttribute("taskDto", taskDto);
        return "task/task_page";
    }

    @GetMapping("/{todoListId}/tasks/{id}/delete")
    public String deleteTask(@PathVariable("todoListId") Long todoListId,
                             @PathVariable(value = "id") Long id) {
        taskService.deleteById(id);
        return "redirect:/users/todolists/" + todoListId;
    }

}
