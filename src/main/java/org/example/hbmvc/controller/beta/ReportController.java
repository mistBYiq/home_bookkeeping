package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.report.ReportCreateRequest;
import org.example.hbmvc.dto.report.ReportDto;
import org.example.hbmvc.dto.report.ReportUpdateRequest;
import org.example.hbmvc.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/users/")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/{userId}/reports")
    public String getAllReportsByUser(@PathVariable("userId") Long id,
                                      Model model) {
        List<ReportDto> reportDtoList = reportService.findAllByUserId(id);
        model.addAttribute("reportDtoList", reportDtoList);
        model.addAttribute("userId", id);
        return "report/reports";
    }

    @GetMapping("/{userId}/reports/createForm")
    public String showCreateReportForm(@PathVariable("userId") Long userId,
                                       Model model) {
        ReportCreateRequest createRequest = new ReportCreateRequest();
        createRequest.setUserId(userId);
        model.addAttribute("createRequest", createRequest);
        return "report/create_report";
    }

    @PostMapping("/reports/create")
    public String createReport(@ModelAttribute("createRequest") ReportCreateRequest createRequest,
                               Model model) {
        ReportDto reportDto = reportService.create(createRequest);
        model.addAttribute("reportDto", reportDto);
        return "report/report_page";
    }


    @GetMapping("/reports/{id}")
    public String findReportById(@PathVariable("id") Long reportId,
                                 Model model) {
        ReportDto reportDto = reportService.findReportById(reportId);
        model.addAttribute("reportDto", reportDto);
        return "report/report_page";
    }

    @GetMapping("/reports/{id}/updateForm")
    public String showUpdateReportFormById(@PathVariable(value = "id") Long reportId,
                                           Model model) {
        ReportDto reportDto = reportService.findReportById(reportId);
        ReportUpdateRequest updateRequest = new ReportUpdateRequest();
        model.addAttribute("reportDto", reportDto);
        model.addAttribute("updateRequest", updateRequest);

        return "report/update_report";
    }

    @PostMapping("/reports/update")
    public String updateReport(@ModelAttribute("updateRequest") ReportUpdateRequest updateRequest,
                                Model model) {
        ReportDto reportDto = reportService.update(updateRequest);
        model.addAttribute("reportDto", reportDto);
        return "report/report_page";
    }

    @GetMapping("/{userId}/reports/{id}/delete")
    public String deleteReport(@PathVariable(value = "userId") Long userId,
                                @PathVariable(value = "id") Long reportId) {

        this.reportService.deleteById(reportId);
        return "redirect:/users/" + userId + "/reports/";
    }
}
