package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.todolist.TodoListCreateRequest;
import org.example.hbmvc.dto.todolist.TodoListDto;
import org.example.hbmvc.dto.todolist.TodoListFullDto;
import org.example.hbmvc.dto.todolist.TodoListUpdateRequest;
import org.example.hbmvc.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/users/")
public class TodoListController {

    @Autowired
    private TodoListService todoListService;

    @GetMapping("/{userId}/todolists")
    public String getAllTodoListByUser(@PathVariable("userId") Long id,
                                       Model model) {
        List<TodoListDto> todoListDto = todoListService.findAllByUserId(id);
        model.addAttribute("todoListDto", todoListDto);
        model.addAttribute("userId", id);
        return "todolist/todolists";
    }

    @GetMapping("/{userId}/todolists/createForm")
    public String showCreateTodoListForm(@PathVariable("userId") Long userId,
                                         Model model) {
        TodoListCreateRequest createRequest = new TodoListCreateRequest();
        createRequest.setUserId(userId);
        model.addAttribute("createRequest", createRequest);
        return "todolist/create_todolist";
    }

    @PostMapping("/todolists/create")
    public String createTodoList(@ModelAttribute("createRequest") TodoListCreateRequest createRequest,
                                 Model model) {
        TodoListFullDto todoListDto = todoListService.create(createRequest);
        model.addAttribute("todoListDto", todoListDto);
        return "todolist/todolist_page";
    }


    @GetMapping("/todolists/{id}")
    public String findTodoListById(@PathVariable("id") Long todoListId,
                                   Model model) {
        TodoListFullDto todoListDto = todoListService.findTodoListFullById(todoListId);
        model.addAttribute("todoListDto", todoListDto);
        return "todolist/todolist_page";
    }

    @GetMapping("/todolists/{id}/updateForm")
    public String showUpdateTodoListFormById(@PathVariable(value = "id") Long todoListId,
                                             Model model) {
        TodoListFullDto todoListDto = todoListService.findTodoListFullById(todoListId);
        TodoListUpdateRequest updateRequest = new TodoListUpdateRequest();
        model.addAttribute("todoListDto", todoListDto);
        model.addAttribute("updateRequest", updateRequest);

        return "todolist/update_todolist";
    }

    @PostMapping("/todolists/update")
    public String updateTodoList(@ModelAttribute("updateRequest") TodoListUpdateRequest updateRequest,
                                 Model model) {
        TodoListFullDto todoListDto = todoListService.update(updateRequest);
        model.addAttribute("todoListDto", todoListDto);
        return "todolist/todolist_page";
    }

    @GetMapping("/{userId}/todolists/{id}/delete")
    public String deleteTodoList(@PathVariable(value = "userId") Long userId,
                                 @PathVariable(value = "id") Long todoListId) {

        this.todoListService.deleteById(todoListId);
        return "redirect:/users/" + userId + "/todolists/";
    }
}
