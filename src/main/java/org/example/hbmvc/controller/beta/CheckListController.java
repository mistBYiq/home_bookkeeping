package org.example.hbmvc.controller.beta;

import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.dto.checklist.CheckListCreateRequest;
import org.example.hbmvc.dto.checklist.CheckListDto;
import org.example.hbmvc.dto.checklist.CheckListFullDto;
import org.example.hbmvc.dto.checklist.CheckListUpdateRequest;
import org.example.hbmvc.service.CheckListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users/")
public class CheckListController {

    @Autowired
    private CheckListService checkListService;

    @GetMapping("/test/{userId}/checklists/")
    public String viewCheckLists(Model model) {
        return findUserCheckLists(1, "Name", "asc", model);
    }

    @GetMapping("/test/{userId}/checklists/page/{pageNo}")
    public String findUserCheckLists(@PathVariable(value = "pageNo") int pageNo,
                                     @RequestParam("sortField") String sortField,
                                     @RequestParam("sortDir") String sortDir,
                                     Model model) {
        int pageSize = 5;

        Page<CheckList> page = checkListService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<CheckList> checkLists = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("checkList", checkLists);
        return "/check/checklists";
    }

    @GetMapping("/{userId}/checklists")
    public String findAllCheckListsForUser(@PathVariable("userId") Long userId,
                                         Model model) {
        List<CheckListDto> checkListDtoList = checkListService.findAllByUserId(userId);
        model.addAttribute("checkListDtoList", checkListDtoList);
        model.addAttribute("userId", userId);
        return "check/checks";
    }

    @GetMapping("/{userId}/checklists/createForm")
    public String showCreateCheckListForm(@PathVariable("userId") Long userId,
                                          Model model) {
        CheckListCreateRequest createRequest = new CheckListCreateRequest();
        createRequest.setUserId(userId);
        model.addAttribute("createRequest", createRequest);
        return "check/create_check";
    }

    @PostMapping("/checklists/create")
    public String createCheckList(@ModelAttribute("createRequest") @Valid CheckListCreateRequest createRequest,
                                  BindingResult result,
                                  Model model) {

        System.out.println("In Create");
        System.out.println(result.toString());
        if (result.hasErrors()) {
            model.addAttribute("createRequest", createRequest);
            return "check/create_check";
        }
        System.out.println("checked ");
        CheckListFullDto checkListDto = checkListService.create(createRequest);

        model.addAttribute("checkListDto", checkListDto);
        return "check/check_page";
    }

    @GetMapping("/checklists/{id}")
    public String findCheckListById(@PathVariable("id") Long id,
                                    Model model) {
        CheckListFullDto checkListDto = checkListService.findCheckListFullById(id);
        model.addAttribute("checkListDto", checkListDto);
        return "check/check_page";
    }

    @GetMapping("/checklists/{id}/updateForm")
    public String showUpdateCheckListFormById(@PathVariable("id") Long checkListId,
                                              Model model) {

        CheckListDto checkListDto = checkListService.findCheckListById(checkListId);
        CheckListUpdateRequest updateRequest = new CheckListUpdateRequest();
        model.addAttribute("checkListDto", checkListDto);
        model.addAttribute("updateRequest", updateRequest);

        return "check/update_check";
    }

    @PostMapping("/checklists/update")
    public String updateCheckList(@ModelAttribute("updateRequest") CheckListUpdateRequest updateRequest,
                                  Model model) {
        CheckListFullDto checkList = checkListService.update(updateRequest);
        model.addAttribute("checkListDto", checkList);
        return "check/check_page";
    }

    @GetMapping("/{userId}/checklists/{id}/delete")
    public String deleteCheckList(@PathVariable(value = "userId") Long userId,
                                  @PathVariable(value = "id") Long checkListId) {

        checkListService.deleteById(checkListId);
        return "redirect:/users/" + userId + "/checklists";
    }
}
