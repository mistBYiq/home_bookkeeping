package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.item.ItemCreateRequest;
import org.example.hbmvc.dto.item.ItemDto;
import org.example.hbmvc.dto.item.ItemUpdateRequest;
import org.example.hbmvc.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/users/buylists/")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/{buyListId}/items/createForm")
    public String showCreateItemForm(@PathVariable("buyListId") Long buyListId,
                                     Model model) {

        ItemCreateRequest createRequest = new ItemCreateRequest();
        createRequest.setBuyListId(buyListId);
        model.addAttribute("createRequest", createRequest);
        return "item/create_item";
    }

    @PostMapping("/items/create")
    public String createItem(@ModelAttribute("createRequest") ItemCreateRequest createRequest,
                             Model model) {
        itemService.create(createRequest);
        return "redirect:/users/buylists/" + createRequest.getBuyListId();
    }


    @GetMapping("/items/{id}")
    public String findItemById(@PathVariable("id") Long id,
                               Model model) {
        ItemDto itemDto = itemService.findById(id);
        model.addAttribute("itemDto", itemDto);
        return "item/item_page";
    }

    @GetMapping("/items/{id}/updateForm")
    public String showUpdateItemForm(@PathVariable("id") Long id,
                                     Model model) {
        ItemDto itemDto = itemService.findById(id);
        ItemUpdateRequest updateRequest = new ItemUpdateRequest();
        model.addAttribute("itemDto", itemDto);
        model.addAttribute("updateRequest", updateRequest);
        return "/item/update_item";
    }

    @PostMapping("/items/update")
    public String updateItem(@ModelAttribute("updateRequest") ItemUpdateRequest updateRequest,
                             Model model) {
        ItemDto itemDto = itemService.update(updateRequest);
        model.addAttribute("itemDto", itemDto);

        return "item/item_page";
    }

    @GetMapping("/{buyListId}/items/{id}/delete")
    public String deleteItem(@PathVariable("buyListId") Long buyListId,
                             @PathVariable(value = "id") Long id) {
        itemService.deleteById(id);
        return "redirect:/users/buylists/" + buyListId;

    }
}
