package org.example.hbmvc.controller.beta;

import org.example.hbmvc.dto.subcategory.SubcategoryCreateRequest;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;
import org.example.hbmvc.dto.subcategory.SubcategoryFullDto;
import org.example.hbmvc.dto.subcategory.SubcategoryUpdateRequest;
import org.example.hbmvc.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/categories/")
public class SubcategoryController {

    @Autowired
    private SubcategoryService subcategoryService;

    @GetMapping("/{categoryId}/subcategories")
    public String findAllSubcategoriesByCategory(@ModelAttribute("categoryId") Long categoryId,
                                                 Model model) {
        List<SubcategoryDto> subcategoryDtoList = subcategoryService.findAllByCategoryId(categoryId);
        model.addAttribute("subcategoryDtoList", subcategoryDtoList);
        return "subcategory/subcategories";
    }

    @GetMapping("/subcategories")
    public String findAllSubcategories(Model model) {

        List<SubcategoryDto> subcategoryDtoList = subcategoryService.findAll();
        model.addAttribute("subcategoryDtoList", subcategoryDtoList);

        return "subcategory/subcategories";
    }

    @GetMapping("/{categoryId}/subcategories/createForm")
    public String showCreateSubcategoryForm(@ModelAttribute("categoryId") Long categoryId,
                                            Model model) {
        SubcategoryCreateRequest createRequest = new SubcategoryCreateRequest();
        createRequest.setCategoryId(categoryId);
        model.addAttribute("createRequest", createRequest);

        return "subcategory/create_subcategory";
    }

    @PostMapping("/subcategories/create")
    public String createSubcategory(@ModelAttribute("createRequest") SubcategoryCreateRequest createRequest) {
        subcategoryService.create(createRequest);
        return "redirect:/categories/" + createRequest.getCategoryId();
    }

    @GetMapping("/subcategories/{id}")
    public String findCSubcategoryById(@PathVariable("id") Long id,
                                       Model model) {

        SubcategoryFullDto subcategoryDto = subcategoryService.findById(id);
        model.addAttribute("subcategoryDto", subcategoryDto);

        return "subcategory/subcategory_page";
    }

    @GetMapping("/subcategories/{id}/updateForm")
    public String showUpdateSubcategoryFormById(@PathVariable(value = "id") Long id,
                                                Model model) {
        SubcategoryDto subcategoryDto = subcategoryService.findById(id);
        SubcategoryUpdateRequest updateRequest = new SubcategoryUpdateRequest();
        model.addAttribute("updateRequest", updateRequest);
        model.addAttribute("subcategoryDto", subcategoryDto);

        return "subcategory/update_subcategory";
    }

    @PostMapping("/subcategories/update")
    public String updateSubcategory(@ModelAttribute("updateRequest") SubcategoryUpdateRequest updateRequest,
                                    Model model) {
        subcategoryService.update(updateRequest);
        return "redirect:/categories/" + updateRequest.getCategoryId();

    }

    @GetMapping("/{categoryId}/subcategories/{id}/delete")
    public String deleteCategory(@PathVariable(value = "categoryId") Long categoryId,
                                 @PathVariable(value = "id") Long id) {

        subcategoryService.deleteById(id);
        return "redirect:/categories/" + categoryId;
    }
}
