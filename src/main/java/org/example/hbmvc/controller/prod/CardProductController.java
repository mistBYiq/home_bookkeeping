package org.example.hbmvc.controller.prod;

import org.example.hbmvc.dto.cardproduct.CardFindQuery;
import org.example.hbmvc.dto.cardproduct.CardProductCreateRequest;
import org.example.hbmvc.dto.cardproduct.CardProductDto;
import org.example.hbmvc.dto.cardproduct.CardProductFullDto;
import org.example.hbmvc.dto.cardproduct.CardProductUpdateRequest;
import org.example.hbmvc.dto.category.CategoryDto;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;
import org.example.hbmvc.service.CardProductService;
import org.example.hbmvc.service.CategoryService;
import org.example.hbmvc.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/cardproducts")
public class CardProductController {

    @Autowired
    private CardProductService cardProductService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubcategoryService subcategoryService;

    @GetMapping("/")
    public String findAllProduct(@ModelAttribute("query") CardFindQuery query,
                                 Model model) {
        List<CardProductDto> cardProductDtoList = cardProductService.findAllByNameLike(query.getQuery());
//        List<CardProductDto> cardProductDtoList = cardProductService.findAll();
//        CardFindQuery query = new CardFindQuery();
        model.addAttribute("query", query);
        model.addAttribute("cardDtoList", cardProductDtoList);
        return "card/cards";
    }

//    @GetMapping("/find")
//    public String findAllByQuery(@ModelAttribute("query") CardFindQuery query,
//                                 Model model) {
//        List<CardProductDto> cardProductDtoList = cardProductService.findAllByNameLike(query.getQuery());
//        model.addAttribute("query", query);
//        model.addAttribute("cardDtoList", cardProductDtoList);
//        return "card/cards";
//    }

    @GetMapping("/new/")
    public String selectCategory(Model model) {
        List<CategoryDto> categoryDtoList = categoryService.findAll();

        model.addAttribute("categoryDtoList", categoryDtoList);
        return "card/select_category";
    }

    @GetMapping("/new/select-category/{name}")
    public String selectSubcategory(@PathVariable("name") String categoryName,
                                    Model model) {
        List<SubcategoryDto> subcategoryDtoList = subcategoryService.findAllByCategoryName(categoryName);
        model.addAttribute("subcategoryDtoList", subcategoryDtoList);
        return "card/select_subcategory";
    }

    @GetMapping("/new/select-subcategory/{name}")
    public String showCreateForm(@PathVariable("name") String subcategoryName,
                                 Model model) {
        CardProductCreateRequest createRequest = new CardProductCreateRequest();
        createRequest.setSubcategoryName(subcategoryName);
        model.addAttribute("createRequest", createRequest);

        return "card/create_card";
    }

    @PostMapping("/create")
    public String createCardProduct(@ModelAttribute("createRequest") CardProductCreateRequest createRequest) {

        cardProductService.create(createRequest);
        return "redirect:/cardproducts/";
    }

    @GetMapping("/{id}")
    public String findCardProductFullDto(@PathVariable("id") Long id,
                                         Model model) {
        CardProductFullDto cardProductDto = cardProductService.findProductFullDtoById(id);

        model.addAttribute("cardProductDto", cardProductDto);
        return "card/card_page";
    }

    @GetMapping("/{id}/updateForm/")
    public String showCardProductUpdateForm(@PathVariable("id") Long id,
                                            Model model) {
        CardProductDto cardProductDto = cardProductService.findProductDtoById(id);
        CardProductUpdateRequest updateRequest = new CardProductUpdateRequest();
        model.addAttribute("cardProductDto", cardProductDto);
        model.addAttribute("updateRequest", updateRequest);

        return "card/update_card";
    }

    @PostMapping("/update")
    public String updateCardProduct(@ModelAttribute("updateRequest") CardProductUpdateRequest updateRequest) {
        cardProductService.update(updateRequest);
        return "redirect:/cardproducts/";
    }

    @GetMapping("/{id}/delete")
    public String deleteCardProduct(@PathVariable("id") Long id) {
        cardProductService.deleteById(id);
        return "redirect:/cardproducts/";
    }

}
