package org.example.hbmvc.controller.prod;

import org.example.hbmvc.dto.cardproduct.CardFindQuery;
import org.example.hbmvc.dto.cardproduct.CardProductCreateRequest;
import org.example.hbmvc.dto.cardproduct.CardProductDto;
import org.example.hbmvc.dto.category.CategoryDto;
import org.example.hbmvc.dto.goods.GoodsCreateRequest;
import org.example.hbmvc.dto.goods.GoodsDto;
import org.example.hbmvc.dto.goods.GoodsFullDto;
import org.example.hbmvc.dto.goods.GoodsUpdateRequest;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;
import org.example.hbmvc.service.CardProductService;
import org.example.hbmvc.service.CategoryService;
import org.example.hbmvc.service.GoodsService;
import org.example.hbmvc.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/users/checklists/")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private CardProductService cardProductService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubcategoryService subcategoryService;

    @GetMapping("/{checkListId}/select-cardproducts/")
    public String selectCardProductForGoods(@PathVariable("checkListId") Long checkListId,
                                            @ModelAttribute("query") CardFindQuery query,
                                            Model model) {
        return selectCardProductForGoodsFindPaginated(checkListId, 1, query, "Name", "asc", model);
    }

    @GetMapping("/{checkListId}/select-cardproducts/page/{pageNo}")
    public String selectCardProductForGoodsFindPaginated(@PathVariable("checkListId") Long checkListId,
                                                         @PathVariable("pageNo") int pageNo,
                                                         @ModelAttribute("query") CardFindQuery query,
                                                         @RequestParam("sortField") String sortField,
                                                         @RequestParam("sortDir") String sortDir,
                                                         Model model) {
        int pageSize = 10;

        Page<CardProductDto> page = cardProductService.findAllByNameLikePaging(query.getQuery(), pageNo, pageSize, sortField, sortDir);
        List<CardProductDto> cardProductDtoList = page.getContent();
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("query", query);
        model.addAttribute("cardDtoList", cardProductDtoList);
        model.addAttribute("checkListId", checkListId);
        return "goods/select_card";
    }

//    @GetMapping("/{checkListId}/select-cardproducts/")
//    public String selectCardProductForGoods(@PathVariable("checkListId") Long checkListId,
//                                            @ModelAttribute("query") CardFindQuery query,
//                                            Model model) {
//        List<CardProductDto> cardProductDtoList = cardProductService.findAllByNameLike(query.getQuery());
//        model.addAttribute("query", query);
//        model.addAttribute("cardDtoList", cardProductDtoList);
//        model.addAttribute("checkListId", checkListId);
//        return "goods/select_card";
//    }

    @GetMapping("/{checkListId}/cardproducts/new")
    public String selectCategory(@PathVariable("checkListId") Long checkListId,
                                 Model model) {
        List<CategoryDto> categoryDtoList = categoryService.findAll();

        model.addAttribute("categoryDtoList", categoryDtoList);
        model.addAttribute("checkListId", checkListId);
        return "goods/select_category";
    }

    @GetMapping("/{checkListId}/cardproducts/new/select-category/{name}")
    public String selectSubcategory(@PathVariable("name") String categoryName,
                                    @PathVariable("checkListId") Long checkListId,
                                    Model model) {
        List<SubcategoryDto> subcategoryDtoList = subcategoryService.findAllByCategoryName(categoryName);
        model.addAttribute("subcategoryDtoList", subcategoryDtoList);
        model.addAttribute("checkListId", checkListId);
        return "goods/select_subcategory";
    }

    @GetMapping("/{checkListId}/cardproducts/new/select-subcategory/{name}")
    public String showCreateForm(@PathVariable("name") String subcategoryName,
                                 @PathVariable("checkListId") Long checkListId,
                                 Model model) {
        CardProductCreateRequest createRequest = new CardProductCreateRequest();
        createRequest.setSubcategoryName(subcategoryName);
        model.addAttribute("createRequest", createRequest);
        model.addAttribute("checkListId", checkListId);

        return "goods/create_card";
    }

    @PostMapping("/{checkListId}/cardproducts/create")
    public String createCardProduct(@PathVariable("checkListId") Long checkListId,
                                    @ModelAttribute("createRequest") CardProductCreateRequest createRequest,
                                    Model model) {

        CardProductDto cardProductDto = cardProductService.createCardProduct(createRequest);
        model.addAttribute("cardProductDto", cardProductDto);
        model.addAttribute("checkListId", checkListId);

        return "goods/card_page";
    }


    @GetMapping("/{checkListId}/goods/createForm/{name}")
    public String showCreateGoodsForm(@PathVariable("checkListId") Long checkListId,
                                      @PathVariable("name") String cardProductName,
                                      Model model) {

        GoodsCreateRequest createRequest = new GoodsCreateRequest();
        createRequest.setCheckListId(checkListId);
        createRequest.setCardProductName(cardProductName);
        model.addAttribute("createRequest", createRequest);
        return "goods/create_goods";
    }

    @PostMapping("/goods/create")
    public String createGoods(@ModelAttribute("product") GoodsCreateRequest request) {
        goodsService.create(request);
        return "redirect:/users/checklists/" + request.getCheckListId();
    }

    @GetMapping("/{checkListId}/goods")
    public String findAllGoodsByCheckId(@PathVariable("checkId") Long checkListId,
                                        Model model) {
        List<GoodsDto> goodsDtoList = goodsService.findAllGoodsByCheckListId(checkListId);
        model.addAttribute("GoodsDtoList", goodsDtoList);
        model.addAttribute("checkListId", checkListId);
        return "goods/goods";
    }


    @GetMapping("/goods/{id}")
    public String findGoodsFullDtoById(@PathVariable("id") Long goodsId,
                                       Model model) {
        GoodsFullDto goodsDto = goodsService.findGoodsFullDtoById(goodsId);
        model.addAttribute("goodsDto", goodsDto);

        return "goods/goods_page";
    }


    @GetMapping("/goods/{id}/updateForm/")
    public String showGoodsUpdateForm(@PathVariable("id") Long id,
                                      Model model) {
        GoodsFullDto goodsDto = goodsService.findGoodsFullDtoById(id);
        GoodsUpdateRequest updateRequest = new GoodsUpdateRequest();
        model.addAttribute("goodsDto", goodsDto);
        model.addAttribute("updateRequest", updateRequest);
        return "goods/update_goods";
    }

    @PostMapping("/goods/update")
    public String updateProductFromCheckList(@ModelAttribute("updateRequest") GoodsUpdateRequest updateRequest) {
        goodsService.update(updateRequest);
        return "redirect:/users/checklists/goods/" + updateRequest.getId();
    }

    @GetMapping("/{checkListId}/goods/{id}/delete/")
    public String deleteProductFromCheckList(@PathVariable("checkListId") Long checkListId,
                                             @PathVariable("id") Long id) {
        goodsService.deleteById(id);
        return "redirect:/users/checklists/" + checkListId;
    }
}
