package org.example.hbmvc.controller;

import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.todolist.TodoListDto;
import org.example.hbmvc.dto.user.UserCreateRequest;
import org.example.hbmvc.dto.user.UserDto;
import org.example.hbmvc.dto.user.UserUpdateRequest;
import org.example.hbmvc.service.BuyListService;
import org.example.hbmvc.service.CheckListService;
import org.example.hbmvc.service.TodoListService;
import org.example.hbmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final CheckListService checkService;

    private final BuyListService buyListService;

    private final TodoListService todoListService;

    public UserController(UserService userService,
                          CheckListService checkService,
                          BuyListService buyListService,
                          TodoListService todoListService) {
        this.userService = userService;
        this.checkService = checkService;
        this.buyListService = buyListService;
        this.todoListService = todoListService;
    }

    @GetMapping("/")
    public String viewHomePage(Model model) {
        return findPaginated(1, "Name", "asc", model);
    }

    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                @RequestParam("sortField") String sortField,
                                @RequestParam("sortDir") String sortDir,
                                Model model) {
        int pageSize = 3;

        Page<UserDto> page = userService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<UserDto> userList = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("userList", userList);
        return "/user/users";
    }

    @GetMapping("/{id}")
    public String findUserById(@PathVariable(value = "id") Long id,
                               Model model,
                               Principal principal) {
//        UserDto userDto = userService.findUserById(id);
        UserDto userDto = userService.findUserByEmail(principal.getName());

//        int counterAllChecks = checkService.;
        List<BuyListDto> favoritesBuyList = buyListService.findAllFavoritesByUserId(id);
        List<TodoListDto> favoritesTodoList = todoListService.findAllFavoritesByUserId(id);

        model.addAttribute("userDto", userDto);
        model.addAttribute("favoritesBuyList", favoritesBuyList);
        model.addAttribute("favoritesTodoList", favoritesTodoList);

        return "/user/userpage";
    }

    @GetMapping("/createForm")
    public String showCreateUserForm(Model model) {
        model.addAttribute("user", new UserCreateRequest());
        return "user/create_user";
    }

    @PostMapping("/create")
    public String createUser(@ModelAttribute("user") UserCreateRequest user) {
        userService.create(user);
        return "redirect:/users/";
    }

    @GetMapping("/{id}/updateForm/")
    public String showUpdateUserFormById(@PathVariable("id") Long id,
                                         Model model) {
        UserDto userDto = userService.findUserById(id);
        UserUpdateRequest updateRequest = new UserUpdateRequest();

        model.addAttribute("userDto", userDto);
        model.addAttribute("updateRequest", updateRequest);
        return "user/update_user";
    }

    @PostMapping("/update/")
    public String updateUser(@ModelAttribute("request") UserUpdateRequest request) {
        userService.update(request);
        return "redirect:/users/" + request.getId();
    }

    @GetMapping("/{id}/delete/")
    public String deleteUserById(@PathVariable(value = "id") Long id) {
        this.userService.deleteById(id);
        return "redirect:/users/";
    }

}
