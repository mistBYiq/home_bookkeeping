package org.example.hbmvc.controller;

import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.checklist.CheckListDto;
import org.example.hbmvc.dto.income.IncomeDto;
import org.example.hbmvc.dto.report.ReportDto;
import org.example.hbmvc.dto.todolist.TodoListDto;
import org.example.hbmvc.service.BuyListService;
import org.example.hbmvc.service.CheckListService;
import org.example.hbmvc.service.IncomeService;
import org.example.hbmvc.service.ReportService;
import org.example.hbmvc.service.TodoListService;
import org.example.hbmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/users/")
public class CustomController {

    @Autowired
    private UserService userService;

    @Autowired
    private CheckListService checkListService;

    @Autowired
    private BuyListService buyListService;

    @Autowired
    private TodoListService todoListService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private IncomeService incomeService;


    @GetMapping("/{id}/finance")
    public String getFinancePage(@PathVariable("id") Long userId,
                                 Model model) {
        List<CheckListDto> allCheckDtoList = checkListService.findAllByUserId(userId);
        List<IncomeDto> allIncomeDtoList = incomeService.findAllByUserId(userId);
        List<ReportDto> allReportDtoList = reportService.findAllByUserId(userId);

        model.addAttribute("userId", userId);
        model.addAttribute("checkListDto", allCheckDtoList);
        model.addAttribute("incomeDtoList", allIncomeDtoList);
        model.addAttribute("reportDtoList", allReportDtoList);

        model.addAttribute("expenseTotalSum", checkListService.countAllTotalPriceByUserId(userId));

        model.addAttribute("expenseYearSum", checkListService.countSumPerYearByUserId(userId));
        model.addAttribute("expenseMonthSum", checkListService.countSumPerMonthByUserId(userId));
        model.addAttribute("expenseWeekSum", checkListService.countSumPerWeekByUserId(userId));

        model.addAttribute("incomeTotalSum", incomeService.countAllTotalSumByUserId(userId));

        return "/user/finance";
    }

    @GetMapping("/{id}/planning")
    public String getPlanningPage(@PathVariable("id") Long userId,
                                  Model model) {
        List<BuyListDto> allBuyList = buyListService.findAllByUserId(userId);
        List<TodoListDto> allTodoList = todoListService.findAllByUserId(userId);

        model.addAttribute("userId", userId);
        model.addAttribute("buyListDto", allBuyList);
        model.addAttribute("todoListDto", allTodoList);

        return "/user/planning";
    }
}
