package org.example.hbmvc.domain.enums;

public enum TaskStatus {

    STARTED("STARTED"),
    IN_PROGRESS("IN_PROGRESS"),
    COMPLETED("COMPLETED"),
    NOT_SELECTED("NOT_SELECTED"),
    CANCELED("CANCELED");

    private final String representation;

    TaskStatus(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
