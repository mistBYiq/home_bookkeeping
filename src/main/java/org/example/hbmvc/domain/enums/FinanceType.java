package org.example.hbmvc.domain.enums;

public enum FinanceType {

    INCOME("INCOME"),
    EXPENSE("EXPENSE");

    private final String representation;

    FinanceType(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
