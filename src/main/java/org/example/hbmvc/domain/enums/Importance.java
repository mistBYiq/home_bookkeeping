package org.example.hbmvc.domain.enums;

public enum Importance {
    LOW("LOW"),
    NORMAL("NORMAL"),
    HIGH("HIGH");

    private final String representation;

    Importance(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
