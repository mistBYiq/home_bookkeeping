package org.example.hbmvc.domain.enums;

public enum RoleName {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_MODERATOR("ROLE_MODERATOR"),
    ROLE_USER("ROLE_USER");

    private final String representation;

    RoleName(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
