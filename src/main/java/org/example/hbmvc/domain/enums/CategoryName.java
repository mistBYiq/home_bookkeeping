package org.example.hbmvc.domain.enums;

public enum CategoryName {
    PRODUCTS("PRODUCTS"),
    ENTERTAINMENT("ENTERTAINMENT"),
    SUBJECTS("SUBJECTS"),
    SERVICES("SERVICES"),
    HEALTHS("HEALTHS"),
    BEAUTY("BEAUTY"),
    NOT_SELECTED("NOT_SELECTED");

    private final String representation;

    CategoryName(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
