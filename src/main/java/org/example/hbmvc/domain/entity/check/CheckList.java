package org.example.hbmvc.domain.entity.check;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.user.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
@Table(name = "check_lists")
public class CheckList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "place")
    private String place;

    @Column(name = "purchase_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate purchaseDate;

    @Column(name = "total_price", scale = 2, precision = 12)
    private BigDecimal totalPrice;

    @Column(name = "discount", scale = 2, precision = 12)
    private BigDecimal discount;

    @Column(name = "created", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime updated;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;

    @OneToMany(mappedBy = "checkList",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Goods> goodsList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonManagedReference
    private User user;

    public void addGoods(Goods goods) {
        goodsList.add(goods);
        goods.setCheckList(this);
    }

    public void removeGoods(Goods goods) {
        goodsList.remove(goods);
        goods.setCheckList(null);
    }

    public BigDecimal getTotalSumOfAllPositions() {
        return countSum(this.goodsList);
    }

    private BigDecimal countSum(List<Goods> goodsList) {
        BigDecimal result = new BigDecimal(BigInteger.ZERO);
        for (Goods goods : goodsList) {
            result = result.add(
                    goods.getPrice()
                            .multiply(BigDecimal.valueOf(goods.getAmount())));
        }
        return result;
    }
}
