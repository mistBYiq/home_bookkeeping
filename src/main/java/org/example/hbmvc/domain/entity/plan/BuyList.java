package org.example.hbmvc.domain.entity.plan;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.user.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
@Accessors(chain = true)
@Table(name = "buylists")
public class BuyList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "created", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime updated;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;

    @Column(name = "is_done", nullable = false)
    private Boolean isDone;

    @Column(name = "is_favourites", nullable = false)
    private Boolean isFavourites;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonManagedReference
    private User user;

    @OneToMany(mappedBy = "buyList", cascade = CascadeType.ALL)
    @JsonBackReference
    private List<Item> items = new ArrayList<>();

    @Column(name = "total_price", scale = 2, precision = 12)
    private BigDecimal totalPrice;

    private void addItem(Item item) {
        this.items.add(item);
    }

    private void deleteItem(Item item) {
        this.items.remove(item);
    }

    public BigDecimal getTotalPrice() {
        return countSum(this.items);
    }

    private BigDecimal countSum(List<Item> items) {
        BigDecimal result = new BigDecimal(BigInteger.ZERO);
        for (Item item : items) {
            result = result.add(
                    item.getPrice()
                            .multiply(
                                    BigDecimal.valueOf(item.getAmount())
                            )
            );
        }
        return result;
    }

}
