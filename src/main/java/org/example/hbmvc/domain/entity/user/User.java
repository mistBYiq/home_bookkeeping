package org.example.hbmvc.domain.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.domain.entity.fin.Report;
import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.domain.entity.plan.TodoList;
import org.hibernate.Hibernate;
import org.hibernate.annotations.BatchSize;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@Accessors(chain = true)
@Table(name = "users") //, uniqueConstraints = @UniqueConstraint(columnNames = "email")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "surname", nullable = false, length = 30)
    private String surname;

    @Column(name = "email", nullable = false, length = 320, unique = true) //{64}@{255}=320
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "created", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime updated;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;

    @Column(name = "is_blocked", nullable = false)
    private Boolean isBlocked;

    /*-------------------------------------------*/

    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JsonBackReference
//    @BatchSize(size = 20)
    private List<BuyList> buyLists = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonBackReference
    private List<CheckList> checks = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private List<Income> incomes = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private List<Report> reports = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private List<TodoList> todos = new ArrayList<>();


    /*-------------------------------------------*/


    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "l_users_roles",
            joinColumns = @JoinColumn(name = "user_id",
                    nullable = false,
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    nullable = false,
                    referencedColumnName = "id"))
    @JsonBackReference
    private Collection<UserRole> roles;

    public User() {
    }

    public User(String name,
                String surname,
                String email,
                String password,
                Collection<UserRole> roles) {
        super();
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }


    public String getFullName() {
        return this.name + " " + this.surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;

        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return 562048007;
    }
}
