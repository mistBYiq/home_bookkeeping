package org.example.hbmvc.domain.entity.check;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.CardProduct;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
@Accessors(chain = true)
@Table(name = "goods")
public class Goods implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price", nullable = false, scale = 2, precision = 10)
    private BigDecimal price;

    @Column(name = "price_without_discount", nullable = false, scale = 2, precision = 10)
    private BigDecimal priceWithoutDiscount;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "weight_in_grams")
    private Integer weightInGrams;

    @Column(name = "milliliters")
    private Integer milliliters;

    @Column(name = "composition")
    private String composition;

    @Column(name = "made_in")
    private String madeIn;

    @Column(name = "detail_one")
    private String detailOne;

    @Column(name = "detail_two")
    private String detailTwo;

    @Column(name = "detail_three")
    private String detailThree;

    @Column(name = "created", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime updated;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "card_products_id")
    @JsonManagedReference
    private CardProduct cardProduct;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "check_list_id")
    private CheckList checkList;

}
