package org.example.hbmvc.domain.entity.prod;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.Goods;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
@Accessors(chain = true)
@Table(name = "card_products")
public class CardProduct implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "counter_use")
    private Integer counterUse;

    @Column(name = "created", updatable = false, columnDefinition = "timestamp default CURRENT_TIMESTAMP not null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;

    @Column(name = "updated", nullable = false, columnDefinition = "timestamp default CURRENT_TIMESTAMP not null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm yyyy-MM-dd")
    private LocalDateTime updated;

    @Column(name = "is_deleted", nullable = false, columnDefinition = "boolean default false not null")
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "subcategory_name", referencedColumnName = "name")
    @JsonManagedReference
    private Subcategory subcategory;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "l_card_products_specifications",
            joinColumns = @JoinColumn(name = "card_product_id"),
            inverseJoinColumns = @JoinColumn(name = "specification_id"))
    private List<Specification> specifications = new ArrayList<>();

    @OneToMany(mappedBy = "cardProduct", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonBackReference
    private List<Goods> goodsList = new ArrayList<>();
}
