package org.example.hbmvc.dto.buylist;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BuyListCreateRequest {
    private String name;
    private String description;
    private Long userId;
}
