package org.example.hbmvc.dto.buylist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.dto.item.ItemDto;

import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BuyListFullDto extends BuyListDto {
    private List<ItemDto> items;

    public static BuyListFullDto toDto(BuyList entity) {
        BuyListFullDto dto = new BuyListFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setUsername(entity.getUser().getFullName());
        dto.setUserId(entity.getUser().getId());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setIsDone(entity.getIsDone());
        dto.setIsFavorites(entity.getIsFavourites());
        dto.setTotalPrice(entity.getTotalPrice());
        dto.setItems(entity.getItems().stream()
                .map(ItemDto::toDto)
                .collect(Collectors.toList()));
        return dto;
    }
}
