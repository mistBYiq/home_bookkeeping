package org.example.hbmvc.dto.buylist;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.BuyList;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class BuyListDto {
    private Long id;
    private String name;
    private String description;
    private String username;
    private Long userId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;
    private Boolean isDone;
    private Boolean isFavorites;
    private BigDecimal totalPrice;

    public static BuyListDto toDto(BuyList entity) {
        BuyListDto dto = new BuyListDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setUsername(entity.getUser().getFullName());
        dto.setUserId(entity.getUser().getId());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setIsDone(entity.getIsDone());
        dto.setIsFavorites(entity.getIsFavourites());
        dto.setTotalPrice(entity.getTotalPrice());

        return dto;
    }
}
