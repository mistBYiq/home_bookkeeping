package org.example.hbmvc.dto.buylist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BuyListUpdateRequest extends BuyListCreateRequest {
    private Long id;
    private Boolean isFavorites;
}
