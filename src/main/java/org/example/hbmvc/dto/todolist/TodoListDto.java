package org.example.hbmvc.dto.todolist;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.TodoList;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TodoListDto {
    private Long id;
    private String name;
    private String description;
    private String username;
    private Long userId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;
    private Boolean isDone;
    private Boolean isFavorites;

    public static TodoListDto toDto(TodoList entity) {
        TodoListDto dto = new TodoListDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setUsername(entity.getUser().getFullName());
        dto.setUserId(entity.getUser().getId());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setIsDone(entity.getIsDone());
        dto.setIsFavorites(entity.getIsFavourites());

        return dto;
    }
}
