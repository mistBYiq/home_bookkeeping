package org.example.hbmvc.dto.todolist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TodoListUpdateRequest extends TodoListCreateRequest {
    private Long id;
    private Boolean isDone;
    private Boolean isFavorites;
}
