package org.example.hbmvc.dto.todolist;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TodoListCreateRequest {
    private String name;
    private String description;
    private Long userId;
}
