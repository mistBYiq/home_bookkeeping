package org.example.hbmvc.dto.todolist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.TodoList;
import org.example.hbmvc.dto.task.TaskDto;

import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TodoListFullDto extends TodoListDto {
    private List<TaskDto> taskList;

    public static TodoListFullDto toDto(TodoList entity) {
        TodoListFullDto dto = new TodoListFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setUserId(entity.getUser().getId());
        dto.setUsername(entity.getUser().getFullName());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setIsDone(entity.getIsDone());
        dto.setIsFavorites(entity.getIsFavourites());
        dto.setTaskList(entity.getTaskList().stream()
                .map(TaskDto::toDto)
                .collect(Collectors.toList()));
        return dto;
    }
}
