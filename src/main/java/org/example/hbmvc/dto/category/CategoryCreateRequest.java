package org.example.hbmvc.dto.category;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CategoryCreateRequest {
    private String name;
    private String description;
}
