package org.example.hbmvc.dto.category;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CategoryUpdateRequest extends CategoryCreateRequest {
    private Long id;

}
