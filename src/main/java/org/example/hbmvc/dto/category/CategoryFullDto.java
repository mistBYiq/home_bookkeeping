package org.example.hbmvc.dto.category;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.Category;
import org.example.hbmvc.dto.subcategory.SubcategoryDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CategoryFullDto extends CategoryDto {
    private String description;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;
    private List<SubcategoryDto> subcategories;

    public static CategoryFullDto toDto(Category entity) {
        CategoryFullDto dto = new CategoryFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
//        dto.setCreated(entity.getCreated());
//        dto.setUpdated(entity.getUpdated());
//        dto.setIsDeleted(entity.getIsDeleted());

        dto.setSubcategories(entity.getSubCategories().stream()
                .map(SubcategoryDto::toDto)
                .collect(Collectors.toList()));
        return dto;
    }
}
