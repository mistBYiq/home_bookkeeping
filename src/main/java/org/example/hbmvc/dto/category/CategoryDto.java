package org.example.hbmvc.dto.category;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.Category;

@Data
@Accessors(chain = true)
public class CategoryDto {
    private Long id;
    private String name;
    private String description;

    public static CategoryDto toDto(Category entity) {
        CategoryDto dto = new CategoryDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());

        return dto;
    }
}
