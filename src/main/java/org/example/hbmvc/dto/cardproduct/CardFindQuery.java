package org.example.hbmvc.dto.cardproduct;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CardFindQuery {
    private String query;
}
