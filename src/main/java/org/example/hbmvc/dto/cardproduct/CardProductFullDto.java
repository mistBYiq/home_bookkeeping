package org.example.hbmvc.dto.cardproduct;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.CardProduct;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CardProductFullDto extends CardProductDto {

    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static CardProductFullDto toDto(CardProduct entity) {
        CardProductFullDto dto = new CardProductFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSubcategoryName(entity.getSubcategory().getName());
        dto.setCategoryName(entity.getSubcategory().getCategory().getName());
        dto.setDescription(entity.getDescription());
        dto.setCounterUse(entity.getCounterUse());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());

        return dto;
    }
}
