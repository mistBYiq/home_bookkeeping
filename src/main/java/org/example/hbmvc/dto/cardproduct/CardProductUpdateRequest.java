package org.example.hbmvc.dto.cardproduct;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.dto.cardproduct.CardProductCreateRequest;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CardProductUpdateRequest extends CardProductCreateRequest {
    private Long id;
}
