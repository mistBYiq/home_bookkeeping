package org.example.hbmvc.dto.cardproduct;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CardProductCreateRequest {
    private String name;
    private String description;
    private String subcategoryName;
}
