package org.example.hbmvc.dto.cardproduct;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.CardProduct;

@Data
@Accessors(chain = true)
public class CardProductDto {
    private Long id;
    private String name;
    private String description;
    private Integer counterUse;
    private String subcategoryName;
    private String categoryName;

    public static CardProductDto toDto(CardProduct entity) {
        CardProductDto dto = new CardProductDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setCounterUse(entity.getCounterUse());
        dto.setSubcategoryName(entity.getSubcategory().getName());
        dto.setCategoryName(entity.getSubcategory().getCategory().getName());

        return dto;
    }
}
