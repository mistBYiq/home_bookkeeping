package org.example.hbmvc.dto.checklist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CheckListUpdateRequest extends CheckListCreateRequest {
    private Long id;
}
