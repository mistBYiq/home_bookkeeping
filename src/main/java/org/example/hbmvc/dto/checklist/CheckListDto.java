package org.example.hbmvc.dto.checklist;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.CheckList;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class CheckListDto {
    private Long id;
    private String name;
    private String description;
    private String place;
    private String username;
    private Long userId;
    private LocalDate purchaseDate;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;
    private BigDecimal totalPrice;
    private BigDecimal totalSumOfAllPositions;

    public static CheckListDto toDto(CheckList entity) {
        CheckListDto dto = new CheckListDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setPlace(entity.getPlace());
        dto.setUsername(entity.getUser().getFullName());
        dto.setUserId(entity.getUser().getId());
        dto.setPurchaseDate(entity.getPurchaseDate());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());

        //
        dto.setTotalPrice(entity.getTotalPrice());
        dto.setTotalSumOfAllPositions(entity.getTotalSumOfAllPositions());

        return dto;
    }
}
