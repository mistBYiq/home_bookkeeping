package org.example.hbmvc.dto.checklist;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class CheckListCreateRequest {
    @NotNull
    @NotEmpty(message = "Name can't be null (for example write the name of the store)")
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    @NotEmpty(message = "Description can't be null (e.g. official company name)")
    @Size(min = 1, max = 255)
    private String description;

    @NotNull
    @NotEmpty(message = "Address can't be null (e.g. address of the store - city, street)")
    @Size(min = 1, max = 255)
    private String place;

    @NotNull
    @NotEmpty(message = "Total Price can't be null (e.g. 100 or 100.00 or 100,00")
    @Pattern(message = "price must be in the format - 100 or 100.00 or 100,00",
            regexp = "^$|^(\\d{1,12}(?:[\\.\\,]\\d{1,2})?)$")
    @Size(min = 1, max = 15)
    private String totalPrice;

    @NotNull
    @NotEmpty(message = "The Purchase Date  can't be null")
    @Size(min = 8, max = 10)
    @Pattern(message = "The Purchase Date must be in the format  dd.MM.yyyy or dd/MM/yyyy  (for example:  01.01.2021)",
            regexp = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$")
    private String date;
    private Long userId;
}
