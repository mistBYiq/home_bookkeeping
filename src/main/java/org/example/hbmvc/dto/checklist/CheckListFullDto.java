package org.example.hbmvc.dto.checklist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.dto.goods.GoodsDto;

import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CheckListFullDto extends CheckListDto {
    private List<GoodsDto> goodsList;

    public static CheckListFullDto toDto(CheckList entity) {
        CheckListFullDto dto = new CheckListFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setPlace(entity.getPlace());
        dto.setUsername(entity.getUser().getFullName());
        dto.setUserId(entity.getUser().getId());
        dto.setPurchaseDate(entity.getPurchaseDate());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());

        //
        dto.setTotalPrice(entity.getTotalPrice());
        dto.setTotalSumOfAllPositions(entity.getTotalSumOfAllPositions());
        dto.setGoodsList(entity.getGoodsList().stream()
                .map(GoodsDto::toDto)
                .collect(Collectors.toList()));

        return dto;
    }



}
