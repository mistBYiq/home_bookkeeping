package org.example.hbmvc.dto.checklist;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class CheckStatisticsDto {
    private String name;
    private String surname;
    private int totalChecks;
    private BigDecimal totalSum;
    private int checksInThisYear;
    private BigDecimal sumThisYear;
    private int checksInThisMonth;
    private BigDecimal sumThisMonth;
}
