package org.example.hbmvc.dto.report;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ReportUpdateRequest extends ReportCreateRequest {
    private Long id;
}
