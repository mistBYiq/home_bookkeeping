package org.example.hbmvc.dto.report;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.fin.Report;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class ReportDto {
    private Long id;
    private String name;
    private String timeline;
    private Long userId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static ReportDto toDto(Report entity) {
        ReportDto dto = new ReportDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setTimeline(entity.getTimeline());

        dto.setUserId(entity.getUser().getId());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());

        return dto;
    }
}
