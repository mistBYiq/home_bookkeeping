package org.example.hbmvc.dto.report;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ReportFullDto extends ReportDto {
//    private List<ProductModel> products;
//
//    public static CheckListFullModel toModel(CheckList entity) {
//        CheckListFullModel model = new CheckListFullModel();
//        model.setId(entity.getId());
//        model.setName(entity.getName());
//        model.setDescription(entity.getDescription());
//        model.setPlace(entity.getPlace());
//        model.setOwnerId(entity.getOwner().getId());
//        model.setCreated(entity.getCreated());
//        model.setUpdated(entity.getUpdated());
//        model.setIsDeleted(entity.getIsDeleted());
//        model.setTotalPrice(entity.getTotalPrice());
//        model.setProducts(entity.getGoods().stream()
//                .map(ProductModel::toModel)
//                .collect(Collectors.toList()));
//        return model;
//    }
}
