package org.example.hbmvc.dto.report;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ReportCreateRequest {
    private String name;
    private String timeline;
    private Long userId;
}
