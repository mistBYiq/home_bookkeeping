package org.example.hbmvc.dto.subcategory;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SubcategoryUpdateRequest extends SubcategoryCreateRequest {
    private Long id;
}
