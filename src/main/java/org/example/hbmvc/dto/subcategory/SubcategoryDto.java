package org.example.hbmvc.dto.subcategory;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.Subcategory;

@Data
@Accessors(chain = true)
public class SubcategoryDto {
    private Long id;
    private String name;
    private String description;
    private String categoryName;
    private Long categoryId;

    public static SubcategoryDto toDto(Subcategory entity) {
        SubcategoryDto dto = new SubcategoryDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setCategoryName(entity.getCategory().getName());
        dto.setCategoryId(entity.getCategory().getId());

        return dto;
    }
}
