package org.example.hbmvc.dto.subcategory;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SubcategoryCreateRequest {
    private String name;
    private String description;
    private String categoryName;
    private Long categoryId;
}
