package org.example.hbmvc.dto.subcategory;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.prod.Subcategory;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SubcategoryFullDto extends SubcategoryDto {
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static SubcategoryFullDto toDto(Subcategory entity) {
        SubcategoryFullDto dto = new SubcategoryFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setCategoryName(entity.getCategory().getName());
        dto.setCategoryId(entity.getCategory().getId());
//        dto.setCreated(entity.getCreated());
//        dto.setUpdated(entity.getUpdated());
//        dto.setIsDeleted(entity.getIsDeleted());

        return dto;
    }
}
