package org.example.hbmvc.dto.income;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class IncomeUpdateRequest extends IncomeCreateRequest {
    private Long id;
}
