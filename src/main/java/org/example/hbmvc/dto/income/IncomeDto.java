package org.example.hbmvc.dto.income;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.fin.Income;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class IncomeDto {
    private Long id;
    private String name;
    private String description;
    private BigDecimal sum;
    private LocalDateTime created;
    private LocalDate date;
    private Long userId;

    public static IncomeDto toDto(Income entity) {
        IncomeDto dto = new IncomeDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setSum(entity.getSum());
        dto.setDate(entity.getDate());
        dto.setCreated(entity.getCreated());
        //  dto.setDate(entity.getDate());
        dto.setUserId(entity.getUser().getId());

        return dto;
    }
}
