package org.example.hbmvc.dto.income;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.fin.Income;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class IncomeFullDto extends IncomeDto {
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static IncomeFullDto toDto(Income entity) {
        IncomeFullDto dto = new IncomeFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setSum(entity.getSum());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
       // dto.setDate(entity.getDate());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setUserId(entity.getUser().getId());

        return dto;
    }
}
