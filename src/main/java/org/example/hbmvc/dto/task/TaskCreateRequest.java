package org.example.hbmvc.dto.task;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TaskCreateRequest {
    private String name;
    private String description;
    private Long todoListId;
    //   private LocalDateTime planTime;
}
