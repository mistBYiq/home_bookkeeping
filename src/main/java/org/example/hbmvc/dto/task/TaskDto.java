package org.example.hbmvc.dto.task;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.Task;
import org.example.hbmvc.domain.enums.TaskStatus;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TaskDto {
    private Long id;
    private String name;
    private String description;
    private TaskStatus status;
    private Boolean isDone;
    //   private LocalDateTime planTime;
    private Long todoListId;
    private LocalDateTime created;
    private LocalDateTime updated;

    public static TaskDto toDto(Task entity) {
        TaskDto dto = new TaskDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setTodoListId(entity.getTodoList().getId());
        dto.setDescription(entity.getDescription());
        dto.setStatus(entity.getStatus());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDone(entity.getIsDone());
        return dto;
    }
}
