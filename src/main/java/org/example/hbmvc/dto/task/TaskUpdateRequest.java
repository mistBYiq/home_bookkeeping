package org.example.hbmvc.dto.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.enums.TaskStatus;


@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TaskUpdateRequest extends TaskCreateRequest {
    private Long id;
    private Boolean isDone;
    private TaskStatus status;
}
