package org.example.hbmvc.dto.item;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.plan.Item;
import org.example.hbmvc.domain.enums.Importance;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ItemDto {
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private Importance importance;
    private int amount;
    private Long buyListId;

    public static ItemDto toDto(Item entity) {
        ItemDto dto = new ItemDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setPrice(entity.getPrice());
        dto.setBuyListId(entity.getBuyList().getId());
        dto.setAmount(entity.getAmount());
        dto.setImportance(entity.getImportance());
        return dto;
    }
}
