package org.example.hbmvc.dto.item;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ItemUpdateRequest extends ItemCreateRequest {
    private Long id;
}
