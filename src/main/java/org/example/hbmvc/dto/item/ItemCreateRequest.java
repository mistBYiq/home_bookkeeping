package org.example.hbmvc.dto.item;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.enums.Importance;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ItemCreateRequest {
    private String name;
    private String description;
    private BigDecimal price;
    private Importance importance;
    private int amount;
    private Long buyListId;

    public ItemCreateRequest() {
        this.amount = 1;
    }
}
