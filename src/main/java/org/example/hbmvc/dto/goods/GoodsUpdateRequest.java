package org.example.hbmvc.dto.goods;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsUpdateRequest extends GoodsCreateRequest {
    private Long id;
}
