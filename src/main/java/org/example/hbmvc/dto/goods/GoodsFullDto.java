package org.example.hbmvc.dto.goods;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.Goods;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GoodsFullDto extends GoodsDto {

    private Integer weightInGrams;
    private Integer milliliters;
    private String composition;
    private String madeIn;

    private String detailOne;
    private String detailTwo;
    private String detailThree;

    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static GoodsFullDto toDto(Goods entity) {
        GoodsFullDto dto = new GoodsFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPrice(entity.getPrice());
        dto.setPriceWithoutDiscount(entity.getPriceWithoutDiscount());

        dto.setAmount(entity.getAmount());

        dto.setWeightInGrams(entity.getWeightInGrams());
        dto.setMilliliters(entity.getMilliliters());
        dto.setComposition(entity.getComposition());
        dto.setMadeIn(entity.getMadeIn());
        dto.setDetailOne(entity.getDetailOne());
        dto.setDetailTwo(entity.getDetailTwo());
        dto.setDetailThree(entity.getDetailThree());

        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
        dto.setCardProductName(entity.getCardProduct().getName());
        dto.setCheckId(entity.getCheckList().getId());

        return dto;
    }
}
