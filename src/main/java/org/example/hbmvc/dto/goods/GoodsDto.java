package org.example.hbmvc.dto.goods;


import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.check.Goods;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class GoodsDto {
    private Long id;
    private String name;
    private BigDecimal price;
    private BigDecimal priceWithoutDiscount;
    private int amount;

    private String cardProductName;
    private Long checkId;


    public static GoodsDto toDto(Goods entity) {
        GoodsDto dto = new GoodsDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPrice(entity.getPrice());
        dto.setPriceWithoutDiscount(entity.getPriceWithoutDiscount());
        dto.setAmount(entity.getAmount());

        dto.setCardProductName(entity.getCardProduct().getName());
        dto.setCheckId(entity.getCheckList().getId());

        return dto;
    }
}
