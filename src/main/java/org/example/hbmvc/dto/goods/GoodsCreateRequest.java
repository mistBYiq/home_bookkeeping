package org.example.hbmvc.dto.goods;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class GoodsCreateRequest {
    @NotNull
    @NotEmpty(message = "Name can't be null (for example write the name of the store)")
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    @NotEmpty(message = "Total Price can't be null (e.g. 100 or 100.00 or 100,00")
    @Pattern(message = "price must be in the format - 100 or 100.00 or 100,00", regexp = "^$|^(\\d{1,12}(?:[\\.\\,]\\d{1,2})?)$")
    private String price;

    @NotNull
    @NotEmpty(message = "Total Price can't be null (e.g. 100 or 100.00 or 100,00")
    @Pattern(message = "price must be in the format - 100 or 100.00 or 100,00", regexp = "^$|^(\\d{1,12}(?:[\\.\\,]\\d{1,2})?)$")
    private String priceWithoutDiscount;

    @NotNull
    @NotEmpty(message = "Amount can't be null")
    private int amount;

    private Integer weightInGrams;
    private Integer milliliters;
    private String composition;
    private String madeIn;
    private String detailOne;
    private String detailTwo;
    private String detailThree;

    private String cardProductName;
    private Long checkListId;

    public GoodsCreateRequest() {
        this.amount = 1;
    }
}
