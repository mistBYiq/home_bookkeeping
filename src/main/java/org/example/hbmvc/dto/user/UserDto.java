package org.example.hbmvc.dto.user;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.user.User;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class UserDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static UserDto toDto(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());

        return dto;
    }
}
