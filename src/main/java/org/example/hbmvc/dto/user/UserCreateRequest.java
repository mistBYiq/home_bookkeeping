package org.example.hbmvc.dto.user;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserCreateRequest {
    private String name;
    private String surname;
    private String email;
    private String password;
}
