package org.example.hbmvc.dto.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.hbmvc.domain.entity.user.User;
import org.example.hbmvc.dto.buylist.BuyListDto;
import org.example.hbmvc.dto.checklist.CheckListDto;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserFullDto extends UserDto {
    private List<BuyListDto> buyList;
    private List<CheckListDto> checkList;

    public static UserFullDto toDto(User entity) {
        UserFullDto dto = new UserFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
      //  dto.setUserRole(entity.getUserRole().getName().getRepresentation());
        dto.setEmail(entity.getEmail());
    //    dto.setLogin(entity.getLogin());
        dto.setPassword(entity.getPassword());
        dto.setCreated(entity.getCreated());
        dto.setUpdated(entity.getUpdated());
        dto.setIsDeleted(entity.getIsDeleted());
//        dto.setBuyList(entity.getLists().stream()
//                .map(BuyListModel::toModel)
//                .collect(Collectors.toList()));
//        dto.setCheckList(entity.getChecks().stream()
//                .map(CheckListModel::toModel)
//                .collect(Collectors.toList()));
        return dto;
    }
}
