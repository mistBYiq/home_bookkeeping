package org.example.hbmvc.dto.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserUpdateRequest extends UserCreateRequest {
    private Long id;
}
