package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.prod.Category;
import org.example.hbmvc.domain.entity.prod.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubcategoryRepository extends JpaRepository<Subcategory, Long> {
    List<Subcategory> findAllByCategory(Category category);

    Optional<Subcategory> findByName(String name);
}
