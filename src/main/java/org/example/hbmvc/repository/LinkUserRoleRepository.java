package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.user.LinkUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkUserRoleRepository extends JpaRepository<LinkUserRole, Long> {
}
