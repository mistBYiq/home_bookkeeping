package org.example.hbmvc.repository;


import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CheckListRepository extends JpaRepository<CheckList, Long> {

    List<CheckList> findAllByUser(User user);

    List<CheckList> findByUserEmail(String email);

    int countAllChecksByUser(User user);

    @Query(
            value = "SELECT sum(total_price) FROM check_lists c WHERE c.user_id = :id",
            nativeQuery = true)
    double countAllTotalPriceByUserId(@Param("id") Long id);

    @Query (
            value = "select sum(total_price) from check_lists c where c.user_id = :id and c.purchase_date >= date_trunc('month', current_timestamp) and c.purchase_date < date_trunc('month', current_timestamp)  + INTERVAL '1 month'",
            nativeQuery = true
    )
    double countSumPerMonthByUserId(@Param("id") Long id);

    @Query (
            value = "select sum(total_price) from check_lists c where c.user_id = :id and c.purchase_date >= date_trunc('year', current_timestamp) and c.purchase_date < date_trunc('year', current_timestamp)  + INTERVAL '1 year'",
            nativeQuery = true
    )
    double countSumPerYearByUserId(@Param("id") Long id);

    @Query (
            value = "select sum(total_price) from check_lists c where c.user_id = :id and c.purchase_date >= date_trunc('week', current_timestamp) and c.purchase_date < date_trunc('week', current_timestamp)  + INTERVAL '1 week'",
            nativeQuery = true
    )
    double countSumPerWeekByUserId(@Param("id") Long id);
}
