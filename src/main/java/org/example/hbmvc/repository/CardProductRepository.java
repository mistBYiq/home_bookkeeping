package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.prod.CardProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CardProductRepository extends JpaRepository<CardProduct, Long> {
    Optional<CardProduct> findByName(String name);

    List<CardProduct> findAllByNameLike(String query);

    Page<CardProduct> findAllByNameLike(String query, Pageable pageable);
}
