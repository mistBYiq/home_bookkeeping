package org.example.hbmvc.repository;


import org.example.hbmvc.domain.entity.plan.BuyList;
import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuyListRepository extends JpaRepository<BuyList, Long> {
    List<BuyList> findAllByUserAndIsFavouritesIsTrue(User user);

    List<BuyList> findAllByUser(User user);
}
