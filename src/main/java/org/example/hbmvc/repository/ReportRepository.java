package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.fin.Report;
import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findAllByUser(User user);
}
