package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.prod.LinkCardProductSpecification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkCardProductSpecificationRepository extends JpaRepository<LinkCardProductSpecification, Long> {

}
