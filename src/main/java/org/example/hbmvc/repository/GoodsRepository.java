package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.check.CheckList;
import org.example.hbmvc.domain.entity.check.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {
    List<Goods> findGoodsByCheckList(CheckList checkList);
}
