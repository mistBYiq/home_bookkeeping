package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.plan.TodoList;
import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoListRepository extends JpaRepository<TodoList, Long> {
    List<TodoList> findAllByUserAndIsFavouritesIsTrue(User user);

    List<TodoList> findAllByUser(User user);
}
