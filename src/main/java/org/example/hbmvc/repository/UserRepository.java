package org.example.hbmvc.repository;


import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);
    boolean existsById(Long id);
    User findUserByEmail(String email);
    Optional<User> findByEmail(String email);

}
