package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.fin.Income;
import org.example.hbmvc.domain.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Long> {
    List<Income> findAllByUser(User user);
    @Query(
            value = "SELECT sum(sum) FROM incomes c WHERE c.user_id = :id",
            nativeQuery = true)
    double countAllTotalSumByUserId(@Param("id") Long id);
}
