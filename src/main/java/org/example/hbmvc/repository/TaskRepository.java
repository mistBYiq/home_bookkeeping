package org.example.hbmvc.repository;

import org.example.hbmvc.domain.entity.plan.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
