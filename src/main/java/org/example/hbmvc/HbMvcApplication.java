package org.example.hbmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HbMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(HbMvcApplication.class, args);
    }

}