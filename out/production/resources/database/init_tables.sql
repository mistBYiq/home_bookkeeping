create table if not exists buylists
(
    id bigint generated by default as identity
        constraint buylists_pkey
            primary key,
    created timestamp not null,
    description varchar(255),
    is_deleted boolean not null,
    is_done boolean not null,
    is_favourites boolean not null,
    name varchar(100),
    total_price numeric(12,2),
    updated timestamp not null,
    user_id bigint not null
        constraint fk7xdpm5wpwn77q4rmxie4oswnc
            references users
);

alter table buylists owner to postgres;